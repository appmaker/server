#!/usr/bin/perl

#***********************************************************************
# Restricted SCP server prototype for AppMaker
# (to be able to upload/download files via SSH)
#***********************************************************************

# We have to add a line in authorized_keys per user with public key as
# follows:
#     command=".../restricted-scp <user>",no-port-forwarding,
#             no-X11-forwarding,no-agent-forwarding,
#             no-pty <key type> <key value> <comment>
# This way all users will be able to upload/download files using a shared
# account in a server. Options can also be added in global sshd_config
# limiting them to account via Match.

# The list of files a user is able to upload/download must be included in
# following files:
#     .../users/<user>-uploads.cfg
#     .../users/<user>-downloads.cfg
# Each line would be considered a file name, and they would be stored in
# following directories:
#     .../uploads
#     .../downloads
# Each one prefixed by its user id to distinguish which one is the owner.

# It seems there is no RFC describing SCP protocol, just found following
# information:
#     https://blogs.oracle.com/janp/entry/how_the_scp_protocol_works

use strict;
use Cwd qw();
use File::Basename qw();
use Fcntl qw(:flock);

# Configuration parameters
use constant USERS        => 'users';
use constant DOWNLOADS    => 'downloads';
use constant UPLOADS      => 'uploads';
use constant COPY_TIMEOUT => 10;
use constant COPY_BLOCK   => 1024 * 1024;
use constant FILE_LIMIT   => 999999999;

# Predefined constants
use constant COPY_SUCCESS => pack("C*", 0x00);
use constant COPY_WARNING => pack("C*", 0x01);
use constant COPY_FAILURE => pack("C*", 0x02);
use constant COMMAND      => File::Basename::basename($0);
use constant DIRECTORY    => File::Basename::dirname($0);

sub log_record ($$)
{
  my($type, $text)= @_;
  my($sec, $min, $hour, $mday, $mon, $year)= localtime;
  
  chomp($text);
  ++$mon;
  $year+= 1900;
  printf STDLOG "%04u/%02u/%02u %02u:%02u:%02u %s %s -> %s\n",
		$year, $mon, $mday, $hour, $min, $sec, $$, $type, $text;
}

sub log_error ($)
{
  my($text)= @_;
  log_record("ERROR", $text);
}

sub log_warning ($)
{
  my($text)= @_;
  log_record("WARNING", $text);
}

my($saved_descriptors);

sub hide_descriptors ()
{
  unless ($saved_descriptors)
  { # Save standard file handles and make them unbuffered
    open(LNKIN,  "<&STDIN")  and
    open(LNKOUT, ">&STDOUT") and
    open(LNKERR, ">&STDERR") or do
    { log_error("Cannot duplicate standard file handles: $!");
      print STDERR "ssh: Talk to system administrator!\n";
      stop(1);
    };
    select LNKIN;  $| = 1;
    select LNKOUT; $| = 1;
    select LNKERR; $| = 1;
    select STDOUT;
    $saved_descriptors= 1;
  }

  # Redirect standard file handles log file and make them unbuffered
  open(STDIN,  "</dev/null") and
  open(STDOUT, ">&STDLOG")   and
  open(STDERR, ">&STDLOG")   or do
  { log_error("Cannot duplicate standard file handles: $!");
    print LNKERR "ssh: Talk to system administrator!\n";
    stop(1);
  };
  select STDIN;  $| = 1;
  select STDOUT; $| = 1;
  select STDERR; $| = 1;
  select STDOUT;
}

sub expose_descriptors ()
{
  if ($saved_descriptors)
  { # Restore standard file handles and make them unbuffered
    open(STDIN,  "<&LNKIN")  and
    open(STDOUT, ">&LNKOUT") and
    open(STDERR, ">&LNKERR") or do
    { log_error("Cannot duplicate standard file handles: $!");
      print LNKERR "ssh: Talk to system administrator!\n";
      return stop(1);
    };
    select STDIN;  $| = 1;
    select STDOUT; $| = 1;
    select STDERR; $| = 1;
    select STDOUT;
  }
}

sub setup
{
  my($output)= "$0.log";
  my($user);

  # Open log file and make it unbuffered
  open(STDLOG, ">>", $output) or do
  { print STDERR "ssh: Talk to system administrator!\n";
    stop(1);
  };
  select STDLOG; $| = 1; select STDOUT;

  # Keep descriptors safe
  hide_descriptors();

  # Handle signals
  $SIG{INT}= $SIG{TERM}= sub { stop(1); };
  $SIG{PIPE}= sub {};

  # Do initial setup
  log_warning("EXECUTE $0 @ARGV");
  if ($#ARGV eq -1)
  { log_error("Missing user identity in command line");
    print LNKERR "ssh: Talk to system administrator!\n";
    stop(1);
  }
  if ($#ARGV ne 0)
  { log_error("Extra parameters in command line");
    print LNKERR "ssh: Talk to system administrator!\n";
    stop(1);
  }
  $user= $ARGV[0];
  check_user($user) or do
  { log_error("Wrong user identity in command line");
    print LNKERR "ssh: Talk to system administrator!\n";
    stop(1);
  };
  log_warning("Working directory: " . Cwd::cwd());
  log_warning("Environment variables:");
  /^SSH_/ and log_warning("  $_=$ENV{$_}") for (keys %ENV);
  chdir(DIRECTORY) or do
  { log_error("Cannot change directory: " . DIRECTORY);
    log_error($!);
    print LNKERR "ssh: Talk to system administrator!\n";
    stop(1);
  };
  log_warning("Changed directory: " . Cwd::cwd());
  return $user;
}

# Process client command

sub usage ()
{
  print LNKERR "usage: scp <file> <user>@<host>:<file>\n";
  print LNKERR "   or: scp <user>@<host>:<file> <file>\n";
  print LNKERR "   or: ssh <user>@<host> ls uploads\n";
  print LNKERR "   or: ssh <user>@<host> ls downloads\n";
  stop(1);
}

sub check_user ($)
{
  my($user)= @_;

  for ($user)
  { /^[a-zA-Z0-9]+$/ or do
    { log_error("User identity as $_ not allowed");
      return undef;
    };
  }
  return 1;
}

sub check_file ($)
{
  my($file)= @_;

  for ($file)
  { /^[\/\~]/ and do
    { log_error("Absolute path in file $_ not allowed");
      return undef;
    };
    /(^|\/)\.\.(\/|$)/ and do
    { log_error("Directory up in file $_ not allowed");
      return undef;
    };
    /\// and do
    { log_error("Sub-directory in file $_ not allowed");
      return undef;
    };
    /\.zip$/ or do
    { log_error("File $_ not supported");
      return undef;
    };
  }
  return 1;
}

sub exist_file ($@)
{
  my($file, @files)= @_;

  foreach (@files)
  { next unless ($file eq $_);
    return 1;
  }
  return undef;
}

sub load_user ($)
{
  my($file)= @_;
  my($line);
  local(*FILE);
  my(@result);
  
  open(FILE, "<", $file) or do
  { log_error("Cannot open user file $file");
    return ();
  };
  log_warning("Loading user file $file");
  for (;;)
  { $line= <FILE>;
    last unless defined($line);
    chomp($line);
    next unless check_file($line);
    push(@result, $line);
  }
  log_warning("Loaded user file $file");
  close(FILE);
  return @result;
}

sub lock_file ($)
{
  my($file)= @_;
  my($dev, $dev_chk, $ino, $ino_chk);
  local(*FILE);

  open(FILE, ">>", $file) or do
  { log_error("Cannot open file $file");
    close(FILE);
    return undef;
  };
  flock(FILE, LOCK_EX | LOCK_NB) or do
  { log_error("Cannot lock file $file");
    close(FILE);
    return undef;
  };
  ($dev, $ino)= stat(FILE) or do
  { log_error("Cannot access file $file");
    close(FILE);
    return undef;
  };
  ($dev_chk, $ino_chk)= stat($file) or do
  { log_error("Cannot access file $file");
    close(FILE);
    return undef;
  };
  return *FILE if ($dev eq $dev_chk) && ($ino eq $ino_chk);
  close(FILE);
  return undef;
}

sub execute (@)
{
  my(@service)= @_;
  my($code);

  expose_descriptors();
  log_warning("Execute command: @service");
  $code= system(@service);
  hide_descriptors();

  unless ($code == 0)
  { log_error("Command failed: $?");
    print LNKERR "ssh: Talk to system administrator!\n";
    return undef;
  };
  log_warning("Command succeed");
  return 1;
}

sub system_source ($$)
{
  my($file, $real)= @_;

  execute('scp', '-f', $real) or return undef;
  return 1;
}

sub download ($$)
{
  my($user, $file)= @_;
  my(@files)= load_user(USERS . "/$user-" . DOWNLOADS . ".cfg");
  my($real)= "$user-$file";

  # Handle download command
  log_warning("Request to download file $file");
  chdir(DOWNLOADS) or do
  { log_error("Cannot change directory: " . DOWNLOADS);
    log_error($!);
    copy_failure("scp: $file: Talk to system administrator!\n");
    stop(1);
  };
  check_file($file) and
  exist_file($file, @files) or do
  { copy_failure("scp: $file: File not allowed\n");
    stop(1);
  };

  # Execute download command
  custom_source($file, $real) or stop(1);
}

sub send_chunk (*$;$)
{
  my($FILE, $data, $size)= @_;
  my($result);
  
  $size= length($data) unless defined($size);
  $size= length($data) unless ($size < length($data));
  unless ($size > 0)
  { log_error("Empty data for sending");
    return undef;
  }
  $result= syswrite($FILE, $data, $size);
  unless ($result > 0)
  { log_error("Error sending data: $!");
    return undef;
  }
  unless ($result == $size)
  { log_error("Incomplete data sent");
    return undef;
  }
  return 1;
}

sub recv_chunk (*\$$;$)
{
  my($FILE, $data, $size, $timeout)= @_;
  my($result);
  my($fds);

  $timeout= COPY_TIMEOUT unless defined($timeout);
  unless ($size > 0)
  { log_error("Empty data for receiving");
    return undef;
  }

  $fds= '';
  vec($fds, fileno($FILE), 1)= 1;
  $result= select($fds, undef, undef, $timeout);
  unless ($result >= 0) 
  { log_error("Error waiting for data: $!");
    return undef;
  }
  if ($result == 0) 
  { log_error("Time out waiting for data");
    return undef;
  }

  $result= sysread($FILE, $$data, $size);
  defined($result) or do
  { log_error("Error receiving data: $!");
    return undef;
  };
  log_error("No more data to receive") unless ($result > 0);
  return $result;
}

sub copy_success ()
{
  send_chunk(LNKOUT, COPY_SUCCESS) or do
  { log_error("Cannot send SUCCESS status");
    return undef;
  };
  log_warning("SUCCESS status sent");
  return 1;
}

sub copy_failure ($)
{
  my($text)= @_;

  chomp($text);
  send_chunk(LNKOUT, COPY_FAILURE . $text . "\n") or do
  { log_error("Cannot send FAILURE status: $text");
    return undef;
  };
  log_warning("FAILURE status sent: $text");
  return 1;
}

sub complete_status ($)
{
  my($data)= @_;
  my($text);
  my($result);

  if (length($data) > 1)
  { $text= substr($data, 1);
    $data= substr($data, 0, 1);
  }

  for ($data)
  { ($_ eq COPY_WARNING) and do
    { $result= "WARNING";
      last;
    };
    ($_ eq COPY_FAILURE) and do
    { $result= "FAILURE";
      last;
    };
    $result= sprintf("0x%02X", unpack("C*", $data));
    log_error("Unexpected $result status received");
    return;
  }

  for (;;)
  { unless (recv_chunk(LNKIN, $data, 1))
    { log_error("Incomplete $result status received: $text");
      return;
    }
    last if ($data eq "\n");
    $text.= $data;
  }

  log_warning("$result status received: $text");
}
  
sub copy_status ()
{
  my($data, $text);
  my($result);

  recv_chunk(LNKIN, $data, 1) or do
  { log_error("Cannot receive status");
    return undef;
  };
  if ($data eq COPY_SUCCESS)
  { log_warning("SUCCESS status received");
    return 1;
  }
  complete_status($data);
  return undef;
}

sub copy_wrap ($$$)
{
  my($file, $mode, $size)= @_;
  my($data);

  $data= sprintf("C%04o %u %s\n", $mode & 07777, $size, $file);
  send_chunk(LNKOUT, $data) or do
  { log_error("Cannot send file description: $data");
    return undef;
  };
  log_warning("File description sent: $data");
  return 1;
}

sub copy_deliver ($$*)
{
  my($file, $size, $FILE)= @_;
  my($data, $total, $count);

  $total= $size;
  $count= 0;
  while ($count < $total)
  { $size= COPY_BLOCK;
    $size= ($total - $count) if (($count + $size) > $total);
    $size= sysread($FILE, $data, $size);
    unless ($size > 0)
    { log_error("Error after reading $count byte(s) of file $file");
      print LNKERR "scp: $file: Error reading file\n";
      return undef;
    }
    send_chunk(LNKOUT, $data, $size) or do
    { log_error("Error after delivering $count byte(s) of file $file");
      print LNKERR "scp: $file: Error retrieving file\n";
      return undef;
    };
    $count+= $size;
  }

  if (sysread($FILE, $data, 1))
  { log_error("Size changed after delivering whole file $file");
    copy_failure("scp: $file: File size changed\n");
    return undef;
  }
  return undef unless copy_success();
  return 1;
}

sub copy_unwrap ($)
{
  my($file)= @_;
  my($data, $text);
  my($size);

  recv_chunk(LNKIN, $data, 1) or do
  { log_error("Cannot receive protocol message");
    return undef;
  };
  if (($data eq COPY_SUCCESS) ||
      ($data eq COPY_WARNING) ||
      ($data eq COPY_FAILURE))
  { complete_status($data);
    return undef;
  }

  while ($data ne "\n")
  { $text.= $data;
    unless (recv_chunk(LNKIN, $data, 1))
    { log_error("Incomplete protocol message received: $text");
      return undef;
    }
  }

  for ($text)
  { /^C[0-9]+ +([0-9]+)/ or do
    { log_error("Unexpected protocol message received: $text");
      copy_failure("scp: $file: Option not allowed\n");
      return undef;
    };
    $size= $1;
    log_warning("File description received: $_");
  }
  return $size;
}

sub copy_retrieve ($$*)
{
  my($file, $size, $FILE)= @_;
  my($data, $total, $count);

  $total= $size;
  $count= 0;
  while ($count < $total)
  { $size= COPY_BLOCK;
    $size= ($total - $count) if (($count + $size) > $total);
    $size= recv_chunk(LNKIN, $data, $size);
    unless ($size > 0)
    { log_error("Error after retrieving $count byte(s) of file $file");
      print LNKERR "scp: $file: Error delivering file\n";
      return undef;
    }
    unless (syswrite($FILE, $data, $size) == $size)
    { log_error("Error after writing $count byte(s) of file $file");
      print LNKERR "scp: $file: Error writing file\n";
      return undef;
    }
    $count+= $size;
  }

  copy_status() or do
  { log_error("Error after retrieving whole file $file");
    print LNKERR "scp: $file: Error delivering file\n";
    return undef;
  };
  return 1;
}

sub custom_source ($$)
{
  my($file, $real)= @_;
  my($size, $mode, @st);
  local(*FILE);

  return undef unless copy_status();

  open(FILE, "<", "$real") or do
  { log_error("Cannot open file $file");
    copy_failure("scp: $file: Cannot open file\n");
    return undef;
  };

  @st= stat(FILE);
  if ($#st == -1)
  { log_error("Cannot access file $file");
    copy_failure("scp: $file: Cannot access file\n");
    close(FILE);
    return undef;
  };
  $mode= $st[2];
  $size= $st[7];

  copy_wrap($file, $mode, $size) and
  copy_status() and
  copy_deliver($file, $size, FILE) or do
  { close(FILE);
    return undef;
  };
  close(FILE);

  return undef unless copy_status();
  return 1;
}

sub custom_sink ($$$)
{
  my($file, $real, $tmp)= @_;
  my($size);
  local(*TMP);

  return undef unless copy_success();
  $size= copy_unwrap($file);
  return undef unless defined($size);

  log_warning("Expected file $file size: $size");
  if ($size > FILE_LIMIT)
  { log_error("File $file too big");
    copy_failure("scp: $file: File too big\n");
    return undef;
  }

  open(TMP, ">", $tmp) or do
  { log_error("Cannot open file $file");
    copy_failure("scp: $file: Cannot open file\n");
    return undef;
  };

  copy_success() and
  copy_retrieve($file, $size, TMP) or do
  { close(TMP);
    unlink($tmp);
    return undef;
  };
  close(TMP);

  rename($tmp, $real) or do
  { log_error("Cannot save file $file");
    copy_failure("scp: $file: Cannot save file\n");
    unlink($tmp);
    return undef;
  };

  return undef unless copy_success();
  return 1;
}

sub system_sink ($$$)
{
  my($file, $real, $tmp)= @_;

  execute('scp', '-t', $tmp) or do
  { unlink($tmp);
    return undef;
  };
  rename($tmp, $real) or do
  { log_error("Cannot save file $file");
    print LNKERR "scp: $file: Cannot save file\n";
    unlink($tmp);
    return undef;
  };
  return 1;
}

sub upload ($$)
{
  my($user, $file)= @_;
  my(@files)= load_user(USERS . "/$user-" . UPLOADS . ".cfg");
  my($real)= "$user-$file";
  my($tmp, $lock);

  # Handle upload command
  log_warning("Request to upload file $file");
  chdir(UPLOADS) or do
  { log_error("Cannot change directory: " . UPLOADS);
    log_error($!);
    copy_failure("scp: $file: Talk to system administrator!\n");
    stop(1);
  };
  check_file($file) and
  exist_file($file, @files) or do
  { copy_failure("scp: $file: File not allowed\n");
    stop(1);
  };
  (-e $real) and do
  { log_error("File $file already existing");
    copy_failure("scp: $file: File already existing\n");
    stop(1);
  };
  $tmp= $real;
  $tmp=~ s/\.[^\.]*$/.tmp/;
  $lock= lock_file($tmp);
  defined($lock) or do
  { log_error("File $file already uploading");
    copy_failure("scp: $file: File already uploading\n");
    stop(1);
  };
  (-e $real) and do
  { log_error("File $file already existing");
    copy_failure("scp: $file: File already existing\n");
    unlink($tmp);
    stop(1);
  };

  # Execute upload command
  custom_sink($file, $real, $tmp) or stop(1);
}

sub directory ($$)
{
  my($user, $type)= @_;
  my(@files)= sort(load_user(USERS . "/$user-$type.cfg"));
  
  # Handle directory command
  log_warning("Request to list $type");
  chdir($type) or do
  { log_error("Cannot change directory: $type");
    log_error($!);
    print LNKERR "ssh: Talk to system administrator!\n";
    stop(1);
  };

  # Execute directory command
  foreach (@files)
  { my($real)= "$user-$_";
    my(@st)= stat($real);
    my($date, $time, $size);
    if ($#st >= 0)
    { my(@tm)= localtime($st[9]);
      $date= sprintf("%04u/%02u/%02u", $tm[5] + 1900, $tm[4] + 1, $tm[3]);
      $time= sprintf("%02u:%02u:%02u", $tm[2], $tm[1], $tm[0]);
      $size= $st[7];
    }
    else
    { my($tmp)= $real;
      $tmp=~ s/\.[^.]+$/.tmp/;
      @st= stat($tmp);
      $date= '-';
      $time= '-';
      $size= '-';
      $size= $st[7] if ($#st >= 0);
    }
    printf LNKOUT "%-10s %-8s %9s %s\n", $date, $time, $size, $_;
  }
};

sub start ($)
{
  my($user)= @_;

  for ($ENV{SSH_ORIGINAL_COMMAND})
  { $_= "sh" unless defined($_);
    /^scp -f\s+([^\-].*)$/ and do
    { download($user, $1);
      last;
    };
    /^scp -t\s+([^\-].*)$/ and do
    { upload($user, $1);
      last;
    };
    /^ls downloads$/ and do
    { directory($user, DOWNLOADS);
      last;
    };
    /^ls uploads$/ and do
    { directory($user, UPLOADS);
      last;
    };
    log_error("Command $_ not allowed or wrong");
    /^scp\s+(.*)$/ and do
    { my($file)= $1;
      $file=~ s/^-[^\s]*\s*// while ($file =~ /^-/);
      copy_failure("scp: $file: Option not allowed\n");
      stop(1);
    };
    usage();
  }
}

sub stop (;$)
{
  my($code)= @_;
  $code= 0 unless defined($code);
  log_warning("EXIT($code) $0 @ARGV");
  exit($code);
}

do
{
  my($user);
  $user= setup();
  start($user);
  stop();
};
