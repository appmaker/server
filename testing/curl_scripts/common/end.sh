#!/bin/sh

echo ""

if [ -z $ENDPOINT ]; then
  echo "Some errors found... Try again"
  common_help
  usage
else
  URL="$PROTOCOL://$HOST:$PORT/$ENDPOINT"
  if [ -z $PAYLOAD ]; then
    if [ -z $FILESRC ]; then
      CURLQUERY="curl -v -X $METHOD $URL"
    else
      CURLQUERY="curl -v -X $METHOD -F $FILETYPE -F $FILESRC $URL"
    fi
  else
    CURLQUERY="curl -v -X $METHOD -d $PAYLOAD $URL"
  fi

  CURLQUERY="$CURLQUERY --cookie $COOKIE"

  if [ $VERBOSE -eq 1 ]; then
    echo ""
    echo "------------------------- cURL -------------------------------------"
    CURLRESPONSE=`$CURLQUERY`
    echo "------------------------- cURL -------------------------------------"
    echo ""
    echo ""
  else
    CURLRESPONSE=`$CURLQUERY 2>/dev/null`
  fi
  EXITCODE=$?

  # Get curl exit code
  echo "cURL query: $CURLQUERY"
  echo "cURL exit code: $EXITCODE"
  echo "Endpoint: $URL"
  echo "Server response: $CURLRESPONSE"
  echo ""
fi

echo ""
