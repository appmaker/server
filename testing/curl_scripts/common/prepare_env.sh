#!/bin/sh

common_help() {
  echo ""
  echo "Some parameters can be defined as environment variables"
  echo " * HOST - Default=localhost"
  echo " * PORT - Default=4000"
  echo " * PROTOCOL - Default=http"
  echo " * ENDPOINTBASE - Default=appmaker/v1"
  echo ""
}

if [ -z $HOST ]; then
  export HOST=localhost
fi
if [ -z $PORT ]; then
  export PORT=4000
fi
if [ -z $PROTOCOL ]; then
  export PROTOCOL=http
fi
if [ -z $ENDPOINTBASE ]; then
  export ENDPOINTBASE="appmaker/v1"
fi
if [ -z $COOKIE ]; then
  export COOKIE="appmaker_session=appmaker-api-qa-user-1"
fi

if [ -z $VERBOSE ]; then
  export VERBOSE=1
fi

METHOD=GET
PAYLOAD=""
ENDPOINT=""
