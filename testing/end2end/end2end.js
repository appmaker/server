// This smoke-test verifies that the appmaker server installation is sane

'use strict';

var request = require('request'),
    fs = require('fs'),
    Promise = require('promise');

var host = 'localhost',
    port = 4000,
    protocol = 'http',
    base = 'appmaker/v1';

var maxTotalWaitTimeout = 50000,
    maxWaitTimeout = 10000;

if (process.argv.length > 2 &&
    parseInt(process.argv[2]) > 0 && parseInt(process.argv[2]) < 65535) {
  port = parseInt(process.argv[2]);
  debug('Getting port from arguments: ' + port);
} else if (process.env['APPMAKER_PORT'] &&
           parseInt(process.env['APPMAKER_PORT']) > 0 &&
           parseInt(process.env['APPMAKER_PORT']) < 65535) {
  port = parseInt(process.env['APPMAKER_PORT']);
  debug('Getting port from environment: ' + port);
} else {
  debug('Using default listening port: ' + port);
}

////////////////////////////////////////////////////////////////////////////////

function error(msg) {
  function errorTrace(msg) {
    console.log('Error: ' + msg);
  }
  if (Array.isArray(msg)) {
    msg.forEach(errorTrace);
  } else {
    errorTrace(msg);
  }
  process.exit(1);
}

function info(msg) {
  console.log('Info: ' + msg);
}

function debug(msg) {
  console.log('Debug: ' + msg);
}

function ok() {
  process.exit(0);
}

////////////////////////////////////////////////////////////////////////////////

function getEndpoint(resource) {
  return protocol + '://' + host + ':' + port + '/' + base + '/' + resource;
}

function getWorkStatus(workUrl) {
  return new Promise(function (resolve) {
    GET('works/' + workUrl.split('/')[4]).then(function(data) {
      resolve(JSON.parse(data.body));
    });
  });
}

function waitResponse(response) {
  if (response.statusCode == 200) {
    return Promise.resolve();
  }
  if (response.statusCode < 200 || response.statusCode > 299) {
    return Promise.reject('HTTP Error ' + response.statusCode);
  }
  return new Promise(function(resolve, reject) {
    if (response.statusCode == 202) {
      var timeout = setTimeout(function() {
        reject('Too much time waiting for ' + response.headers.location);
      }, maxWaitTimeout);

      // Async query
      var polling = setInterval(function() {
        getWorkStatus(response.headers.location).then(function(status) {
          if (status.progress == 100) {
            clearTimeout(timeout);
            clearInterval(polling);
            resolve();
          }
        }) 
      }, 500);
    } else {
      resolve();
    }
  });
}

function GET(resource) {
  var endpoint = getEndpoint(resource);
  debug('GET ' + endpoint + ' ...');
  return new Promise(function(resolve, reject) {
    request(endpoint, function(error, response, body) {
      if (error) {
        return reject(error);
      }
      waitResponse(response).then(function() {
        resolve({
          response: response,
          body: body
        });
      }, reject);
    });
  });
}

function POST(resource, payload) {
  var endpoint = getEndpoint(resource);
  debug('POST ' + endpoint + ' ...');
  return new Promise(function(resolve, reject) {
    request.post({
      url: endpoint,
      formData: payload
    }, function(error, response, body) {
      if (error) {
        return reject(error);
      }
      waitResponse(response).then(function() {
        resolve({
          response: response,
          body: body
        });
      }, reject);
    });
  });
}

function DELETE(resource) {
  var endpoint = getEndpoint(resource);
  debug('DELETE ' + endpoint + ' ...');
  return new Promise(function(resolve, reject) {
    request.del(endpoint, function(error, response, body) {
      if (error) {
        return reject(error);
      }
      waitResponse(response).then(function() {
        resolve({
          response: response,
          body: body
        });
      }, reject);
    });
  });
}

////////////////////////////////////////////////////////////////////////////////

var totalTimeout = setTimeout(function() {
  error('Too much time !');
  clearTimeout(totalTimeout);
  exit(2);
}, maxTotalWaitTimeout);

var projectId = '',
    channelId = '',
    originId = '';

Promise.all([
  POST('origins', {
    type: 'zip',
    src: {
      value: fs.createReadStream('testapp/application.zip'),
      options: {
        filename: 'application.zip',
        contentType: 'application/zip'
      }
    }
  }),
  POST('projects', {
    name: 'smoke-test',
    description: 'Smoke test project',
    callbackUrl: ''
  })
]).then(function(dataArray) {
  originId = JSON.parse(dataArray[0].body).originId;
  projectId = JSON.parse(dataArray[1].body).projectId;
  info('originId: ' + originId);
  info('projectId: ' + projectId);

  POST('projects/' + projectId + '/channels', {
    name: 'smoke-channel',
    originId: originId,
    deployment: '{}',
    appInfo: '{}',
    description: 'Smoke test channel',
    callbackUrl: ''
  }).then(function(data) {
    channelId = JSON.parse(data.body).channelId;

    info('channelId: ' + channelId);

    DELETE('projects/' + projectId + '/channels/' + channelId).then(function() {
      debug('Channel ' + channelId + ' deleted');
      return DELETE('projects/' + projectId);
    })
    .then(function() {
      debug('Project ' + projectId + ' deleted');
      ok();
    })
    .catch(error);
  }, error);
}, error);
