#!/bin/sh
# Stop Workers based on /etc/init.d scripts

for initFile in `ls /etc/init.d/appmaker-*`; do
  echo "Stopping $initFile"
  $initFile stop
done

echo "All workers stopped!"
exit 0
