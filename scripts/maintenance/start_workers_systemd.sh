#!/bin/sh
# Start Workers based on systemdctl

systemctl start appmaker_worker*
systemctl status appmaker_worker* | egrep 'appmaker_worker|Active'

echo "All workers started!"
exit 0
