#!/bin/sh
# Stop Workers based on PID numbers

for pidFile in `ls /tmp/*Worker.pid /var/run/*Worker.pid`; do
  echo "Killing $pidFile with PID $(cat $pidFile)"
  kill $(cat $pidFile)
done

echo "All workers killed!"
exit 0
