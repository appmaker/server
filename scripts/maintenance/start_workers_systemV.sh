#!/bin/sh
# Start Workers based on /etc/init.d scripts

for initFile in `ls /etc/init.d/appmaker-*`; do
  echo "Stopping $initFile"
  $initFile start
done

echo "All workers started!"
exit 0
