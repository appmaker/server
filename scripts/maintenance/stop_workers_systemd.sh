#!/bin/sh
# Stop Workers based on systemdctl

systemctl stop appmaker_worker*
systemctl status appmaker_worker* | egrep 'appmaker_worker|Active'

echo "All workers stopped!"
exit 0
