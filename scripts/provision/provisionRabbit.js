'use strict'

var log = require('AppMakerLogManager')('RABBIT Provisioning'),
    amqp = require('amqp'),
    workflows = require('./workflowsParser.js')(),
    config = require('AppMakerCFGManager'),
    closing = false,
    amqpExchanges = {},
    amqpQueues = {};

config.init(function onCFGLoaded(cfg) {
  log.debug('Connecting to RabbitMQ virtualhost ' + cfg.rabbit.virtualhost + '...');
  var connection = amqp.createConnection({
    host: cfg.rabbit.host,
    port: cfg.rabbit.port,
    login: cfg.rabbit.userLogin,
    password: cfg.rabbit.userPass,
    vhost: cfg.rabbit.virtualhost,
  });

  connection.on('ready', function() {
    log.debug('Connected to vhost ' + cfg.rabbit.virtualhost + ' on ' +
          cfg.rabbit.host);

    function preProvision(cb) {
      var count = Object.keys(workflows.workflows).length +
                  workflows.flowsteps.length;

      log.debug('Creating queues...');
      workflows.flowsteps.forEach(function(q) {
        log.debug(' - ' + q);
        amqpQueues[q] = connection.queue(q, {
          durable: true,
          autoDelete: false
        }, function(queue) {
          log.debug( ' + (queue) ' + q);
          if (!--count) {
            cb();
          }
        });
      });

      log.debug('Creating exchanges...');
      var exchanges = Object.keys(workflows.workflows);
      exchanges.push(workflows.mainflow);
      exchanges.forEach(function(e) {
        log.debug(' - ' + e);
        connection.exchange(e, {
          type: 'headers',
          durable: true,
          autoDelete: false
        }, function(exchange) {
          amqpExchanges[e] = exchange;
          log.debug( ' + (exchange) ' + e);
          if (!--count) {
            cb();
          }
        });
      });
    }

    ///////////////////////////////////////////////////////////////////////////

    preProvision(function() {
      function bindings(cb) {
        log.debug('Creating bindings...');

        function workflowBinding(workflowName, cb) {
          var wf = workflows.workflows[workflowName];
          log.debug('Processing workflow bindings ' + workflowName +
                ' -> ' + wf.comment);

          var steps = Object.keys(wf.flow);
          var headers = wf.headers;
          headers['x-match'] = 'all';

          // Queues binding (flow steps)
          steps.forEach(function(step) {
            headers = {
              'next-step': step,
              'x-match': 'all'
            };
            amqpQueues[step].bind_headers(amqpExchanges[workflowName],
              headers);
            log.debug(workflowName + ' -> ' + step + ' done !');
          });

          // Bind to the mainflow (entrypoint)
          headers = wf.headers;
          headers['x-match'] = 'all';
          amqpExchanges[workflowName].bind_headers(workflows.mainflow, headers,
            function() {
              log.debug(workflows.mainflow + ' -> ' + workflowName + ' done !');
              cb();
            });
        }

        var wfList = Object.keys(workflows.workflows);
        var wfCount = wfList.length - 1;
        wfList.forEach(function(workflowName) {
          workflowBinding(workflowName, function() {
            if (!--wfCount) {
              cb();
            }
          });
        });
      }

      log.debug('Starting bindings...');
      bindings(function() {
        log.debug('That\'s all folk\'s');
        // Closing connection
        closing = true;
        connection.disconnect()
      });
    });
  });

  connection.on('close', function() {
    log.debug('Connection closed');
    process.exit();
  });

  connection.on('error', function(error) {
    if (!closing)
      log.debug('Connection error: ' + error);
  });
});