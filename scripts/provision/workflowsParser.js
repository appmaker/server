'use strict';

var log = require('AppMakerLogManager')('WORKFLOWS Parser'),
    workflows = require('./data/workflows.json');

module.exports = function _wfParser(verbose) {
  var _namedflows = workflows.namedflows,
      _workflows = {},
      _operations = [],
      _mimeTypes = [],
      _flowSteps = [];

  log.debug('Parsing workflows...');
  Object.keys(workflows.workflows).forEach(function(key) {
    log.debug(' - Workflow ' + key);
    var wf = workflows.workflows[key],
        flow = {};
    
    // Getting supported operations and mimetypes
    if (wf.headers) {
      var h = wf.headers;
      if (h.operation && _operations.indexOf(h.operation) === -1) {
        _operations.push(h.operation);
      }
      if (h.mime && _mimeTypes.indexOf(h.mime) === -1) {
        _mimeTypes.push(h.mime);
      }
    }

    // Processing workflow flow steps
    Object.keys(wf.flow).forEach(function(flowStep) {
      if (Object.keys(_namedflows).indexOf(flowStep) === -1) {
        flow[flowStep] = wf.flow[flowStep];
      } else {
        // Using a namedflow -> Copying flowsteps
        Object.keys(_namedflows[flowStep]).forEach(function(namedFlowStep) {
          flow[namedFlowStep] = _namedflows[flowStep][namedFlowStep];
        });
      }
    });

    // Getting flow steps
    Object.keys(flow).forEach(function (flowStep) {
      if (_flowSteps.indexOf(flowStep) === -1) {
        _flowSteps.push(flowStep);
      }
    });

    // Processed workflow
    _workflows[key] = {
      comment: wf.comment,
      headers: wf.headers,
      flow: flow
    }
  });

  log.debug("Main flow: " + workflows.mainflow);
  log.debug("Supported operations: " + JSON.stringify(_operations));
  log.debug("Supported mime-types: " + JSON.stringify(_mimeTypes));
  log.debug("Flow steps: " + JSON.stringify(_flowSteps));
  log.debug("Workflows: " + JSON.stringify(Object.keys(_workflows)));
  if (verbose) {
    log.debug("Workflows: " + JSON.stringify(_workflows, null, ' '));
  }

  return {
    mainflow: workflows.mainflow,
    workflows: _workflows,
    operations: _operations,
    mimetypes: _mimeTypes,
    flowsteps: _flowSteps
  }
};
