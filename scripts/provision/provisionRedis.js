'use strict';

var log = require('AppMakerLogManager')('REDIS Provisioning'),
    redis = require('redis'),

    origins = require('./data/origins.json'),
    deployments = require('./data/deployments.json'),
    permissions = require('./data/permissions.json'),
    platforms = require('./data/platforms.json'),
    sessions = require('./data/sessions.json'),

    workflows = require('./workflowsParser.js')(),
    config = require('AppMakerCFGManager');

config.init(function onCFGLoaded(cfg) {
  var client = redis.createClient();
  client.select(cfg.redis.database, function() {
    log.debug('Loading permissions ...');
    Object.keys(permissions).forEach(function(key) {
      log.debug(key);
      client.hset('permissions', key, JSON.stringify(permissions[key]));
    });

    log.debug('Loading platforms: ' + JSON.stringify(platforms));
    client.hset('platforms', 'platforms', JSON.stringify(platforms));

    log.debug('Loading origins: ' + JSON.stringify(origins));
    Object.keys(origins).forEach(function(key) {
      log.debug(key);
      client.hset('originTypes', key, JSON.stringify(origins[key]));
    });

    log.debug('Loading deployments: ' + JSON.stringify(deployments));
    Object.keys(deployments).forEach(function(key) {
      log.debug(key);
      client.hset('deploymentTypes', key, JSON.stringify(deployments[key]));
    });

    log.debug('Loading sessions: ' + JSON.stringify(sessions));
    Object.keys(sessions).forEach(function(key) {
      log.debug(key);
      client.hset('sessions', key, JSON.stringify(sessions[key]));
    });

    log.debug('Loading workflows ...');
    client.hset('workflows', 'operations', JSON.stringify(workflows.operations));
    client.hset('workflows', 'mimetypes', JSON.stringify(workflows.mimetypes));
    client.hset('workflows', 'workers', JSON.stringify(workflows.flowsteps));
    client.hset('workflows', 'mainflow', workflows.mainflow);
    client.hset('workflows', 'workflows', JSON.stringify(workflows.workflows));

    process.exit();
  });
});
