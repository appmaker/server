'use strict'

var log = require('AppMakerLogManager')('RABBIT Cleaner'),
    request = require('request'),
    config = require('AppMakerCFGManager');

function rabbitMQRestAPI(method, url, responseCB) {
  request({
    method: method,
    uri: url,
    auth: {
      user: process.configuration.rabbit.adminLogin,
      pass: process.configuration.rabbit.adminPass
    }
  }, responseCB);
}

function cleanExchanges(cfg, cb) {
  var numberExchanges = 0;
  function checkFinish() {
    if (!--numberExchanges) {
      return true;
    }
    return false;
  }
  var baseUri = cfg.restapi + 'exchanges/' + cfg.virtualhost;
  rabbitMQRestAPI('GET', baseUri, function (error, response, body) {
    if (error) {
      return cb(error);
    }
    var exchanges = JSON.parse(body);
    if (Array.isArray(exchanges)) {
      numberExchanges = exchanges.length;
      exchanges.forEach(function(exchange) {
        if (exchange.name !== '' && exchange.name.substring(0,4) !== 'amq.') {
          log.debug(' x Removing exchange ' + exchange.name);
          rabbitMQRestAPI('DELETE', baseUri + '/' +  exchange.name,
            function(error, response, body) {
              log.debug(' * Removed exchange ' + exchange.name);
              checkFinish() && cb();
            });
        } else {
          checkFinish() && cb();
        }
      });
    }
  });
}

function cleanQueues(cfg, cb) {
  var baseUri = cfg.restapi + 'queues/' + cfg.virtualhost;
  rabbitMQRestAPI('GET', baseUri, function (error, response, body) {
    if (error) {
      return cb(error);
    }
    var queues = JSON.parse(body);
    var numberQueues = queues.length;
    if (numberQueues === 0) {
      return cb();
    }
    queues.forEach(function(queue) {
      log.debug(' x Removing queue ' + queue.name);
      rabbitMQRestAPI('DELETE', baseUri + '/' +  queue.name,
        function(error, response, body) {
          log.debug(' * Removed queue ' + queue.name);
          if (!--numberQueues) {
            cb();
          }
        });
    });
  });
}

config.init(function onCFGLoaded(cfg) {
  log.debug('Cleaning old queues and exchanges...');
  cleanExchanges(cfg.rabbit, function() {
    cleanQueues(cfg.rabbit, function() {
      log.debug('That\'s all folk\'s');
      process.exit();
    });
  });
});
