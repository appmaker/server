'use strict';

var log = require('AppMakerLogManager')('REDIS Cleaner'),
    redis = require('redis'),
    config = require('AppMakerCFGManager');

config.init(function onCFGLoaded(cfg) {
  var client = redis.createClient();
  client.select(cfg.redis.database, function() {
    log.debug('Flushing database ' + cfg.redis.database);
    client.flushdb();
    process.exit();
  });
});
