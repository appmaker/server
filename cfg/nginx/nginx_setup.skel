# Virtual hosting

server {
  # domain: Main appmaker domain
  set $domain appmaker.tid.ovh;

  listen        8081;
  server_name   client.$domain;

  location / {
    try_files $uri $uri/ /index.html?/$request_uri;
    root   "/var/lib/jenkins/workspace/AppMaker Client/dist";
    index  index.html;
  }
}

server {
  # domain: Main appmaker domain (also change in at the regex below)
  # root: Folder in which the appmaker leaves the apps
  set $domain appmaker.tid.ovh;
  set $root   /tmp/appmaker/hostedApps;

  listen	8081;
  server_name *.$domain;

  include mime.types;
  types {
    application/x-web-app-manifest+json webapp;
    text/cache-manifest appcache;
  }

  if ($host ~* ^(.*)\.appmaker.tid.ovh$) {
    set $app $1;
  }
  location / {
    try_files $uri $uri/ /index.html?/$request_uri =404;
    root   $root/$app;
    index  index.html index.htm;
  }
}

# Reverse proxy for AppMaker server
# To inform that the server is under maintanance, simply put the file
# index503.html into the document_root folder
# To disable it, you only need to remove index503.html from the root folder
server {
  # domain: Main appmaker domain
  set $domain appmaker.tid.ovh;
  set $root   /tmp/appmaker/server_static;
  root $root;

  listen        8081;
  server_name   $domain;

  client_max_body_size 10G;

  error_page 503 @maintenance;
  location @maintenance {
    rewrite ^(.*)$ /error503.html break;
  }

  location / {
    if (-f $document_root/error503.html) {
      return 503;
    }

    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host:$server_port;
    proxy_pass http://localhost:4000;
  }
}
