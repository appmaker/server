# BetterMobileWeb Server
This is the server side implementation of the BetterMobileWeb suite (named internally as AppMaker).

There are more information regarding the suite in following links:
  * [Product Backlog](https://docs.google.com/document/d/1Ue29Yzjz0j6f-DwxJPaGEOfs5fJq9JExSv7Mo7ehEzY/edit?usp=sharing)
  * [User Experience](https://drive.google.com/folderview?id=0ByQxzecMNRSSflVtNnd6TmlKOGZpMV9aWVRsUHhCWkdjY0pWcThlenNBN0Nhc1pPRFU2UGM&usp=sharing)
  * [Test Cases](https://docs.google.com/spreadsheets/d/19AoBMVw9YnlKzaxQLFIg0fpcBJekGNOfNuxRi7eRyCw/edit?usp=sharing)
  * [Testing Guide](https://docs.google.com/document/d/1xsEhJb6aMeJuR9npH97J2Ek2s1LUbwiIK4dGFIT-r_Q/edit?usp=sharing)

See also the other repositories of the suite: [client side](https://gitlab.com/appmaker/client), [server testing](https://gitlab.com/appmaker/appmaker-rest-api) and [end2end testing](https://gitlab.com/appmaker/selenium-appium).

## Installation

AppMaker server is intended to be installed in Debian systems but actually, any Linux machine can host the system.

### Prerequisites:

Before installing you need to satisfy some system broad dependencies:

  * [Node.js](https://nodejs.org)
  * [Redis](http://redis.io/)
  * [RabbitMQ](https://www.rabbitmq.com/)
  * [Jasmine](https://https://github.com/jasmine/jasmine-npm)
  * [libcairo2-dev](https://packages.debian.org/search?keywords=libcairo2-dev)

### Building

Once you satisfy the dependencies, clone the repository, change dir to it and run:

```bash
$ make
```

This will install the remaining node dependencies for each module of the back-end. In case you want to remove the dependencies use:

```bash
$ make clean
```

If you want to completely reset the state of the repository, run:

```bash
$ make mrproper
```

## Running

The server is mainly composed by two runnable components: the REST API front-end and a set of workers. You can launch everything with:

```bash
$ make run-all
```

If you want to only launch the REST API, use simply:

```bash
$ make run
```

To run only the set of workers, use:

```bash
$ make run-workers
```

You have a [complete list of make targets](#make-commands-summary) at the end of this document.

## Debugging

If you need to debug the node code, install `node-debug`:

```
# npm install -g node-debug
```

Only REST API debugging is available from `make` right now. You run:

```bash
$ make debug
```

And the debugger will be attached to the REST fronend process.

### Inspecting queues

RabbitMQ offers a web interface to see the states of queues but before accessing you need to enable this feature:

As **root**, run:

```
# rabbitmq-plugins enable rabbitmq_management
```

The visit [http://localhost:15672/](http://localhost:15672/) and enter the admin web panel with user/pass: `guest`/`guest`. Now go to the `Admin` section and create a new virtual host and a new user according to the information in the keys `rabbit.virtualhost`, `rabbit.userLogin` and `rabbit.userPass` inside `cfg/defaultConfig.json`.

Next time you want to review the queues simply visit [http://localhost:15672/](http://localhost:15672/)

### Inspecting Redis

Apart from the Redis CLI, several UIs exists. One of the most famous is [Redis Desktop Manager](http://redisdesktop.com/
) but you can opt to [Redis Commander](https://github.com/joeferner/redis-commander), a web based application as well.

## Testing environment

You can pass the tests with:

```bash
$ make tests
```

Testing coverture right now is void and a thorough effort to test every aspect of the back-end is needed.

Testing is performed through Jasmine and the specification files are expected to end in `/[Ss]pec.js/`. Valid names would be `frontend.spec.js`, `frontendSpec.js` or `frontend-spec.js`.

## Distributing

You can build Debian packages by running:

```bash
$ make debian
```

## Deploy and Installation

For running this server, you need to run, not only AppMaker daemons but a reverse-proxy (NGINX) a message queue manager (RabbitMQ) and a database (Redis).

So, as a first step, you should install all these dependencies in the same machine or in other machines inside your network:

* Node.JS
* NGINX
* RabbitMQ
* Redis

Secondly, you should install all appmaker packages you genereated with "make debian"

```bash
$ dpkg -i appmaker_*.deb
```

After that, you should fix the config file in order to provide frontend and workers the addresses and credentials por the message queue and database.

```bash
$ cd /usr/bin/appmaker/frontend
$ edit config.default.json
$ cd ../workers/<workerName>
$ edit config.default.json
```
This file is a JSON file with this contents:

```javascript
{
  "redis": {
    "database": 8
  },

  "rabbit": {
    "host": "localhost",
    "port": 5672,
    "virtualhost": "appmaker",
    "userLogin": "appmaker",
    "userPass": "appmaker",
    "adminLogin": "guest",
    "adminPass": "guest",
    "restapi": "http://localhost:15672/api/"
  },

  "frontend": {
    "port": 4000,
    "oauth2": {
      "github": {
        "client_id": "",
        "client_secret": ""
      }
    }
  },

  "publicUrl": {
    "protocol": "http",
    "host": "domain.tld",
    "port": 80
  },

  "appworker": {
    "paths": {
      "hostedApps": "/tmp/appmaker/hostedApps",
      "workingDir": "/tmp/appmaker/working",
      "targetOriginSubfolder": "content"
    }
  },

  "parallelWorks": 1
}
```

* the "redis" section only defines the database number to use.
* the "rabbit" section has the host and port for rabbitmq server, virtualhost for the appmaker queues and two users, one for using the queues, the other one is the administrator used by the provision scripts (for creating queues).
* the "frontend" section defines the TCP port for listening on and the github application credentials (for user oauth2 login)
* the "publicUrl" defines the URL used for static server after an application is deployed so this information will be included in the application manifest.
* the "appworker" sections includes the absolute paths for appmaker working:
 * hostedApps: Is where static server will look for client applications, so is the forder used to leave the generated application on.
 * workingDir: Is the path used for appmaker works (unzip, icons generation, app generation, ...)
 * targetOriginSubfolder: This is the relative path (from workingDir) used to store the uploaded application.
* the "parallelWorks" defines the maximun number of unacknoledged works the rabbitmq is allowed to deliver to a worker.

Following step is provision the RabbitMQ and Redis database.
Currently, no package is created for this, so you should go to the source code and execute these scripts:

```bash
$ cd scripts/provision
$ make
```

All required queues and basic data will be stored in Rabbit and Redis.

Then, NGINX reverse proxy should be configured, this is a sample config file you can use:

```bash
# This is the proxy used to locate generated static pages under 'hostedApps' folder
# It's necessary to setup a willcard DNS entry for *.domain.tld
server {
  listen        80;
  server_name *.domain.tld;

  include mime.types;
  types {
    application/x-web-app-manifest+json webapp;
  }

  if ($host ~* ^(.*)\.domain.tld$) {
    set $app $1;
  }
  location / {
    try_files $uri $uri/ /index.html?/$request_uri =404;
    root   /tmp/appmaker/hostedApps/$app;
    index  index.html index.htm;
  }
}

# This is the reverse proxy for the appmaker API REST interface running in node.js
server {
  listen        80;
  server_name   appmaker.domain.tld;
    
  client_max_body_size 10G;

  location / {
    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host:$server_port;
    proxy_pass http://localhost:4000;
  }
} 
```

Finally, you can manage the appmaker daemons using systemctl:

```bash
$ systemctl status appmaker*    # will inform you about running status of each worker
$ systemctl stop appmaker_worker_<workername> # Stops workername
$ systemctl start appmaker_worker_<workername> # Starts workername
$ systemctl stop appmaker_frontend # Stops frontend
$ systemctl start appmaker_frontend # Starts frontend
```

NOTE: THIS IS REALLY IMPORTANT TO SETUP A WILLCARD DNS ENTRY FOR USER GENERATED STATIC DOMAINS

## Documentation

A thorough description of the backend is available through the [wiki of the project](https://gitlab.com/appmaker/server/wikis/home).

## Make commands summary

Several commands are available through the top level `Makefile`. This is a summary of every target:

  * `all`: performs a data provisioning and install the server. It runs the tests as well.
  * `data-provision`: fills the data base with the setup information inside `./scripts/provision/data/` and sets RabbitMQ proper queues.
  * `server`: installs the server.
  * `run`: launches the REST front-end. You need to run the workers with `make run-workers` to make the back-end to completely functional.
  * `debug`: launches the REST front-end in debug mode using `node-debug`.
  * `run-workers`: launches the workers.
  * `run-all`: runs the REST front-end and workers.
  * `tests`: run the tests. If a path is provided via the environment variable `TEST_TARGET`, only specifications inside that route are passed.
  * `clean`: undo the installation of back-end by removing installed dependencies. Notice this **does not remove** RabbitMQ queues nor data in the database.
  * `cloc`: count the number of lines.
  * `mrproper`: performs a clean and wipe provisioned data out and performs Git clean commands to delete ignored and untracked files.
  * `dist`: prepare the back-end for creating the Debian packages with `make debian`.
  * `cleandist`: remove the intermediate files and dirs needed for creating the Debian packages.
  * `debian`: generate Debian packages.