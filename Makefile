GIT  ?= git
CLOC ?= cloc
JASMINE ?= jasmine

TEST_TARGET ?= *
ifneq ($(TESTS),)
TEST_TARGET = $(TESTS)
endif

JASMINE_CONFIG_PATH ?= testing/jasmine.json

.PHONY = server

all: data-provision server tests

data-provision: 
	$(MAKE) -C scripts/provision

server:
	$(MAKE) -C src

run:
	$(MAKE) -C src/frontend run

debug:
	$(MAKE) -C src/frontend debug

run-workers:
	$(MAKE) -C src/workers fastrun

## make run-all -j2
run-all: run-workers run

tests:
ifeq ($(TEST_TARGET),*)
	JASMINE_CONFIG_PATH=$(JASMINE_CONFIG_PATH) $(JASMINE)
else
	JASMINE_CONFIG_PATH=$(JASMINE_CONFIG_PATH) $(JASMINE) $(TEST_TARGET)/**/*[Ss]pec.js
endif

clean:
	$(MAKE) -C src clean
	$(MAKE) -C scripts/provision clean

cloc:
	@echo "Counting lines of source code ..."
	@$(CLOC) src/ scripts/

mrproper: clean
	@echo "Cleaning untracked content"
	@$(GIT) clean -df
	@echo "Cleaning ignored content"
	@$(GIT) clean -dXf
	@echo "Cleaning provisioned data"
	$(MAKE) -C scripts/provision mrproper

cleandist:
	@echo "Cleaning dists..."
	$(MAKE) -C src cleandist

dist:
	@echo "Creating dists..."
	$(MAKE) -C src dist

debian:
	@echo "Creating Debian packages..."
	$(MAKE) -C src debian
