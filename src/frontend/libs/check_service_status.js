'use strict';

var config = require('AppMakerCFGManager'),
    redis = require('redis'),
    request = require('request');

var _config = null;
config.init(function onCFGLoaded(cfg) {
  _config = cfg;
});

function checkRedis(cb) {
  var client = redis.createClient();
  client.on('error', function(e) {
    cb({
      status: false,
      error: {
        error: 'Error connecting to Redis',
        reason: e.message
      }
    });
  });

  client.select(_config.redis.database, function() {
    if (client.connected) {
      cb({
        status: client.connected
      });
    } else {
      cb({
        status: false,
        error: {
          error: 'No connection to Redis',
          reason: 'Not connected'
        }
      });
    }
  });
}

function checkRabbit(cb) {
console.log(JSON.stringify(config,null,' '));
  request({
    method: 'GET',
    uri: _config.rabbit.restapi + 'aliveness-test/' + _config.rabbit.virtualhost,
    auth: {
      user: _config.rabbit.adminLogin,
      pass: _config.rabbit.adminPass
    }
  }, function(error, response, body) {
    if (error) {
      cb({
        status: false,
        error: {
          error: 'Request error',
          reason: error
        }
      });
    } else {
      try {
        var data = JSON.parse(body);
        cb(data);
      } catch(e) {
        cb({
          status: false,
          error: {
            error: 'Body parsing error',
            reason: e.message
          }
        });
      }
    }
  });

}

module.exports = function _checkServerStatus(cb) {
  cb = (typeof cb === 'function' ? cb : function() {});

  if (!_config) {
    cb(null, {
      error: 'Not yet initialized'
    });
  }

  var status = {};
  checkRedis(function(value) {
    status['redis'] = value;
    checkRabbit(function(value) {
      value.status = (value.status === 'ok');
      status['rabbit'] = value;
      cb(null, status);
    });
  });
}
