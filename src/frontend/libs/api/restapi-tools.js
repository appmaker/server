'use strict';

var Promise = require('promise'),
    fs = require('fs'),
    ensureSession = require('./sessions');

/**
 * A module with a convenient set of tools to ease the integration with
 * `restapi`.
 *
 * @module restapi-tools
 */

/**
 * @class restapi-tools
 */

/**
 * Returns a loader function for the specified `modulepath`. The loader
 * function will assume all but the `index.js` file in the directory are
 * REST API implementations, one per resource type.
 *
 * When calling the loader, it returns a hash with the resources
 * implementations.
 *
 * It must be used from the `index.js` file of a module:
 * ```js
 * module.exports = require('restapi-tools').makeResourceLoader(__dirname);
 * ```
 *
 * @method
 * @param {modulepath} the path of the file calling the method. The loader will
 * here resource implementations
 * are.
 * @return a loader function for `modulepath`
 */
exports.makeResourceLoader = function (modulepath) {

  return function (libs) {
    var resources =
      fs.readdirSync(modulepath).reduce(function (obj, fileName) {
        if (fileName !== 'index.js') {
          var resourceName = require('path').basename(fileName, '.js');
          var resourcePath = require('path').join(modulepath, fileName);
          var implementation = require(resourcePath);
          if (typeof implementation === 'function') {
            implementation = implementation(libs);
          }
          obj[resourceName] = implementation;
        }
        return obj;
      }, {});

    return resources;
  };
};

/**
 * Decorator to implement REST apis in `restapi` by giving the HTTP
 * implementations of the `post`, `get`, `put` and `delete` methods.
 *
 * @method RESTApi
 * @param {Object} implementation a hash with the HTTP verbs for the endpoint.
 */
exports.RESTApi = function (implementation) {

  var helper = new RESTHelper();

  if (implementation && typeof implementation === 'object') {
    for (var property in implementation) {
      if (implementation.hasOwnProperty(property)) {
        helper[property] = implementation[property];
      }
    }
  }

  return function (req, res, params, method) {
    var end = arguments[arguments.length - 1];
    var boundHelper = Object.create(helper);

    return ensureSession(req, res).then(function (session) {
      boundHelper.setContext({
        request: req,
        response: res,
        parameters: params,
        method: method,
        session: session
      });

      var handler = implementation.call ? implementation :
                    (boundHelper[method.toLowerCase()] || noop);
      var result = handler.call(boundHelper, req, res, params, session);
      if (!result || typeof result.then !== 'function') {
        result = Promise.resolve(result);
      }

      if (this._avoidEnd) { (end = noop); }
      return result
        .then(end.bind(undefined, null))
        .catch(end.bind(undefined));

      function noop() {}
    }.bind(this));
  };
};

/**
 * A collection of useful decorators.
 */
exports.decorators = {
  /**
   * The decorator requires a session to be authenticated. If not, it returns
   * and unauthorized response. If the session is authenticated, then the
   * username is passed as the 4th parameter of the decorated handler instead
   * of the session object.
   */
  authenticated: function (handler) {
    return function (req, res, params, session) {
      var _this = this;
      return session.getData().then(function (sessionData) {
        if (!sessionData || !sessionData.username) {
          return _this.unauthorized('login-required');
        }
        return handler.call(_this, req, res, params, sessionData.username);
      });
    };
  }
};

/**
 * Provides common functionality to implement JSON based HTTP REST APIs.
 *
 * @class RESTHelper
 * @constructor
 */
function RESTHelper() { }

RESTHelper.prototype.setContext = function (context) {
  this.req = context.request;
  this.res = context.response;
  this.params = context.parameters;
  this.method = context.method;
  this.session = context.session;
};

RESTHelper.prototype._send = function (statusCode, json, locationHeader) {
  var body;
  if (json !== undefined) {
    this.res.setHeader('Content-Type', 'application/json');
    body = JSON.stringify(json);
  }
  if (locationHeader) {
    this.res.setHeader('Location', locationHeader);
  }
  this.res.writeHead(statusCode);
  if (body) {
    this.res.write(body);
  }
  return Promise.resolve();
};

RESTHelper.prototype._error = function (statusCode, error) {
  return Promise.reject({ code: statusCode, message: error });
};

var bulkResponses = {
  'ok': 200,
  'created': 201,
  'accepted': 202,
  'notImplemented': 501,
  'unavailable': 503,
  'bad': 400,
  'unauthorized': 401,
  'notFound': 404,
  'conflict': 409,
  'internalError': 500
};

Object.keys(bulkResponses).forEach(function (name, i, keys) {
  var code = bulkResponses[keys[i]];
  if (code >= 200 && code < 400) {
    RESTHelper.prototype[name] = function (json, locationHeader) {
      return this._send(code, json, locationHeader);
    };
  }
  else {
    RESTHelper.prototype[name] = function (error) {
      return this._error(code, error);
    };
  }
});

RESTHelper.prototype.okFile = function (file, name, type, size) {
  if (!fs.existsSync(file)) {
    return this.notFound();
  }

  this._avoidEnd = true;
  this.res.writeHead(200, {
    'Content-Type': type,
    'Content-Length': size,
    'Content-Disposition': 'attachment; filename="' + name  + '"'
  });
  var stream = fs.createReadStream(file);
  return new Promise(function (resolve, reject) {
    var destination = stream.pipe(this.res);
    destination.on('error', reject);
    destination.on('end', resolve);
  }.bind(this));
};

RESTHelper.prototype.urlFor = function (resource, id) {
  var tokens = Array.prototype.slice.call(arguments, 0);
  if (this.namespace) { tokens.unshift(this.namespace); }
  return '/' + tokens.join('/');
};

RESTHelper.prototype.redirect = function (targetUrl) {
  return this._send(302, undefined, targetUrl);
};
