'use strict';

var log = require('AppMakerLogManager')('RESTAPI_V1_ORIGIN'),
    path = require('path'),
    fileHash = require('../common').fileHash,
    RESTApi = require('../restapi-tools').RESTApi,
    authenticated = require('../restapi-tools').decorators.authenticated,
    Promise = require('promise');

fileHash = Promise.denodeify(fileHash);

module.exports = function(libs) {
  var database = libs.database,
      workflow = libs.workflow;

  return {
    origins: RESTApi({
      namespace: 'appmaker/v1',

      options: function () { return Promise.resolve(); },

      post: authenticated(function (req, res, params, username) {
        return fileHash(params.files.src.path).then(function (hash) {
          var originUrl = this.urlFor('origins', hash);
          var workId = workflow.producer.addSingleWork(username, {
            headers: {
              operation: 'neworigin',
              type: params.get('type')
            },
            payload: {
              originId: hash,
              src: params.get('src') // size, path, name, type, mtime
            },
            resourceUrl: originUrl
          });

          if (workId) {
            return this.accepted(
              { originId: hash },
              this.urlFor('works', workId)
            );
          }
          return this.unavailable('Problems creating the workflow!');
        }.bind(this));
      })
    }),

    oneOriginPackage: RESTApi(function (req, res, params) {
      var getOrigin = Promise.denodeify(database.origins.get.bind(database.origins));
      return getOrigin(params.get('originId'))
      .then(function (data) {
        try {
          var file = path.join(data.path, data.src.name);
          var fileName = data.src.name;
          return this.okFile(file, fileName, data.src.type, data.src.size);
        } catch(e) {
          this.unavailable(e.message);
        }
      }.bind(this))
      .catch(function (error) {
        if (error && error.code === 404) {
          return this.notFound(error.message);
        } else {
          return this.unavailable(error);
        }
      }.bind(this));
    }),

    oneOrigin: RESTApi(function (req, res, params) {
      var getOrigin = Promise.denodeify(database.origins.get.bind(database.origins));
      return getOrigin(params.get('originId'))
      .then(function (data) {
        return this.ok(data);
      }.bind(this))
      .catch(function (error) {
        return this.unavailable(error);
      }.bind(this));
    })
  };
};
