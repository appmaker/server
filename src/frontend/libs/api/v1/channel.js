'use strict';

var getHash = require('../common').getHash,
    fileHash = require('../common').fileHash,
    getUUID = require('../common').getUUID,
    fs = require('fs'),
    path = require('path'),
    Promise = require('promise'),
    RESTApi = require('../restapi-tools').RESTApi,
    authenticated = require('../restapi-tools').decorators.authenticated,
    mmm = require('mmmagic'),
    Magic = mmm.Magic;

fileHash = Promise.denodeify(fileHash);

module.exports = function(libs) {
  var database = libs.database,
      workflow = libs.workflow;

  var getProject =
        Promise.denodeify(database.projects.get.bind(database.projects));
  var getChannel =
        Promise.denodeify(database.projects.getChannel.bind(database.projects));
  var allowedImages = /^image\/(gif|png|jpg|jpeg)/;

  function isImageAllowed(iconFile) {
    return new Promise(function (resolve, reject) {
      var magic = new Magic(mmm.MAGIC_MIME_TYPE);
      magic.detectFile(iconFile, function(err, mimeType) {
        if (err) {
          return reject(err);
        }
        var isAllowed = allowedImages.test(mimeType);
        resolve(isAllowed);
      });
    });
  }

  var existDeploymentType = Promise.denodeify(
    database.general.existDeploymentType.bind(database.general)
  );

  // XXX: This is a workaround for issue
  // https://gitlab.com/appmaker/server/issues/70
  // Proper solution is to treat iconUrl as an origin from the client.
  function preprocessIcon(iconData) {
    var iconInfo = {};
    var targetPath = '/tmp/';
    var filename = 'upload-' + getUUID();
    // XXX: Preprocess only if iconUrl contains data
    var isDataUri = /^data:/.test(iconData);
    var preprocessing = !isDataUri ?
        Promise.resolve() :
        saveEmbeddedImage(iconData, targetPath, filename)
        .then(function (iconFile) {
          var notAllowed = Promise.reject('no-format-allowed');
          return isImageAllowed(iconFile.path).then(function (isAllowed) {
            if (isAllowed) {
              return Promise.resolve(iconFile)
            }
            return notAllowed;
          })
          .catch(function () {
            return notAllowed;
          });
        })
        .then(function (fileData) {
          iconInfo.src = {
            name: 'icon.png',
            path: fileData.path,
            size: fileData.size,
            type: fileData.type
          };
          return fileHash(iconInfo.src.path);
        })
        .then(function (hash) {
          iconInfo.hash = hash;
          return iconInfo;
        });

    return preprocessing;
  }

  // TODO: The icon will be received through origins worker in a near future
  // this is a fast solution for embedded images
  function saveEmbeddedImage(imageData, targetPath, targetFileName) {
    var sourceData = imageData.split(',');
    var mime = sourceData[0].split(':')[1].split(';')[0];
    if (sourceData.length != 2) {
      return Promise.reject('No icon provided');
    }
    var targetFile = path.join(
      targetPath,
      targetFileName + '.' + sourceData[0].split(';')[0].substr(-3)
    );
    var imageData = new Buffer(sourceData[1], 'base64');

    return new Promise(function (resolve, reject) {
      fs.writeFile(targetFile, imageData, function(err) {
        if (err) {
          return reject(err);
        }
        var stats = fs.statSync(targetFile);
        resolve({ path: targetFile, size: stats['size'], type: mime });
      });
    });
  }

  function addIconData(api, host, appInfo) {
    return function (iconInfo) {
      if (iconInfo) {
        // TODO: Don't hardcode this with FQDN URLs.
        // Make the modification when requesting the appInfo or
        // from the client.
        appInfo.iconOrigin = iconInfo.hash;
        appInfo.iconUrl =
          'http://' + host + api.urlFor('origins', iconInfo.hash, 'source');
      }
    };
  }

  function processApplicationAndDeploy(channelWorkInfoOnceAdded, workData) {
    channelWorkInfoOnceAdded.then(function () {
      var newAppWorkInfo = workflow.producer.addWork(workData.username, {
        headers: { operation: 'newapp' },
        payload: workData.payload,
        resourceUrl: workData.resourceUrl
      }, workData.metaworkId);
      return newAppWorkInfo.onceAdded;
    })
    .then(function () {
      if (typeof workData.deployment === 'object') {
        var deployAppWorkInfo = workflow.producer.addWork(workData.username, {
          headers: {
            operation: 'deployapp',
            deploymentTypeId: workData.deployment.type
          },
          payload: workData.payload,
          resourceUrl: workData.resourceUrl
        }, workData.metaworkId);
        return deployAppWorkInfo.onceAdded;
      }
    })
    .then(function () {
      workflow.producer.dispatch(workData.metaworkId);
    })
    .catch(function onReject() {
      console.log('Error processing workflow');
    });
  }

  function isInvalidSubdomain(subdomain) {
    var subdomainPattern = '^[A-Za-z0-9]+([A-Za-z0-9-_]*[A-Za-z0-9]+)?$';
    var subDomainRegex = new RegExp(subdomainPattern);
    return !subDomainRegex.test(subdomain);
  }

  function reserveSubdomain(deploymentInfo, previousDeployment) {
    if (deploymentInfo.type === 'appmaker') {
      var subdomain = deploymentInfo.params && deploymentInfo.params.subdomain;
      var previousSubdomain = previousDeployment &&
                              previousDeployment.type === 'appmaker' &&
                              previousDeployment.params &&
                              previousDeployment.params.subdomain;

      return database.subdomains.addSubdomain(subdomain)
        .then(
          function () { return Promise.resolve({ isAvailable: true }); },
          function (error) {
            if (subdomain === previousSubdomain) {
              return Promise.resolve({ isAvailable: true });
            }
            return Promise.resolve({ isAvailable: false, error: error });
          }
        );
    }
  }

  return {
    channels: RESTApi({
      namespace: 'appmaker/v1',

      get: authenticated(function (req, res, params, username) {
        return getProject(username, params.get('projectId'))
        .then(
          function onFulfill(projectData) {
            if (!projectData) {
              return this.notFound('Missing project');
            }
            return this.ok(projectData.channels || {});
          }.bind(this),

          function onReject(error) {
            return this.notFound(error);
          }.bind(this)
        );
      }),

      post: authenticated(function (req, res, params, username) {
        var iconInfo = {};
        var projectId = params.get('projectId');

        var appInfo = params.get('appInfo');
        return preprocessIcon(appInfo && appInfo.iconUrl || '', iconInfo)
        .then(function (info) {
          if (info) { iconInfo = info; }
          return info;
        })
        .then(addIconData(this, req.headers['host'], appInfo))
        .then(function () { return getProject(username, projectId); })
        .then(function (projectData) {
          if (!projectData) {
            return this.notFound('Missing project');
          }
          projectData.channels = projectData.channels || {};

          var name = params.get('name');
          var channelId = getHash(username + '_' + projectId + '_' + name);
          var channelUrl =
            this.urlFor('projects', projectId, 'channels', channelId);
          params.add('channelId', channelId);

          var channels = Object.keys(projectData.channels);
          if (channels.indexOf(channelId) > -1) {
            return this.conflict('Channel exists!');
          }
          if (!name) {
            return this.bad('Missing `name`');
          }

          var deployment = params.get('deployment');
          if (!deployment.type) {
            return this.bad('Missing deploymentType');
          }

          return existDeploymentType(deployment.type)
          .then(function onFulfill(deploymentTypeData) {
            var subdomain = deployment.params && deployment.params.subdomain;

            if (!deploymentTypeData) {
              return this.bad('Invalid deployment type');
            }

            if (deployment.type === 'appmaker' &&
              isInvalidSubdomain(subdomain)){
              return this.bad('Incorrect subdomain format')
            }

            return reserveSubdomain(deployment)
            .then(function (state) {
              if (!state.isAvailable) {
                return this.conflict(state.error);
              }

              var channelWorkInfo = workflow.producer.addWork(username, {
                headers: { operation: 'newchannel' },
                payload: {
                  channel: params.getAll(),
                  project: projectData,
                  originId: iconInfo.hash,
                  src: iconInfo.src
                },
                resourceUrl: channelUrl
              });
              var metaworkId = channelWorkInfo.workId;

              processApplicationAndDeploy(channelWorkInfo.onceAdded, {
                metaworkId: metaworkId,
                payload: {
                  data: params.getAll(),
                  projectId: projectId,
                  channelId: channelId
                },
                resourceUrl: channelUrl,
                username: username,
                deployment: params.get('deployment')
              });

              return this.accepted(
                { channelId: channelId },
                this.urlFor('works', metaworkId)
              );
            }.bind(this));
          }.bind(this),
          function onReject(error) {
            return this.notFound(error);
          }.bind(this));

          }.bind(this),
          function onReject(error) {
            if (error === 'no-format-allowed') {
              return this.bad(error);
            } else {
              return this.notFound(error);
            }
          }.bind(this)
        );
      })
    }),

    oneChannel: RESTApi({
      namespace: 'appmaker/v1',

      get: authenticated(function (req, res, params, username) {
        var projectId = params.get('projectId');
        var channelId = params.get('channelId');
        return getChannel(username, projectId, channelId)
        .then(function onFulfill(channel) {
          return this.ok(channel);
        }.bind(this),

        function onReject(error) {
          return this.notFound(error);
        }.bind(this));
      }),

      put: authenticated(function (req, res, params, username) {
        var iconInfo = {},
            projectId = params.get('projectId'),
            channelId = params.get('channelId');
        var channelUrl =
          this.urlFor('projects', projectId, 'channels', channelId);

        var appInfo = params.get('appInfo');
        return preprocessIcon(appInfo && appInfo.iconUrl || '', iconInfo)
        .then(function (info) {
          if (info) { iconInfo = info; }
          return info;
        })
        .then(addIconData(this, req.headers['host'], appInfo))
        .then(function () { return getProject(username, projectId); })
        .then(
          function onFulfill(projectData) {
            if (!projectData) {
              return this.notFound('Missing project');
            }

            var name = params.get('name');
            if (!name) {
              return this.bad('Missing `name`');
            }
            var channel = projectData.channels.filter(function(channel) {
              return channel.channelId === channelId;
            });
            if (!channel) {
              return this.notFound('Channel doesn\'t exists!');
            }

            var previousDeployment = channel.deployment;
            var currentDeployment = params.get('deployment');
            if (!currentDeployment.type) {
              return this.bad('Missing deploymentType');
            }

            return existDeploymentType(currentDeployment.type)
            .then(function onFulfill(currentDeploymentTypeData) {
              var subdomain = currentDeployment.params &&
                currentDeployment.params.subdomain;

              if (!currentDeploymentTypeData) {
                return this.bad('Invalid deployment type');
              }
              if (currentDeployment.type === 'appmaker' &&
                isInvalidSubdomain(subdomain)){
                return this.bad('Incorrect subdomain format')
              }

              return reserveSubdomain(
                currentDeployment,
                previousDeployment
              )
              .then(function () {
                var channelWorkInfo = workflow.producer.addWork(username, {
                  headers: { operation: 'updatechannel' },
                  payload: {
                    channel: params.getAll(),
                    project: projectData,
                    originId: iconInfo.hash,
                    src: iconInfo.src
                  },
                  resourceUrl: channelUrl
                });

                var metaworkId = channelWorkInfo.workId;
                processApplicationAndDeploy(channelWorkInfo.onceAdded, {
                  metaworkId: metaworkId,
                  payload: {
                    data: params.getAll(),
                    projectId: projectId,
                    channelId: channelId
                  },
                  resourceUrl: channelUrl,
                  username: username,
                  deployment: params.get('deployment')
                });

                return this.accepted(
                  { channelId: channelId },
                  this.urlFor('works', metaworkId)
                );
              }.bind(this));
            }.bind(this),
            function onReject(error) {
              return this.notFound(error);
            }.bind(this));

          }.bind(this),

          function onReject(error) {
            if (error === 'no-format-allowed') {
              return this.bad(error);
            } else {
              return this.notFound(error);
            }
          }.bind(this)
        );
      }),

      'delete': authenticated(function (req, res, params, username) {
        var projectId = params.get('projectId'),
            channelId = params.get('channelId');

        return getProject(username, projectId)
        .then(
          function onFulfill(projectData) {
            if (!projectData) {
              return this.notFound('Missing project');
            }
            var channel = projectData.channels.filter(function(channel) {
              return channel.channelId === channelId;
            });
            if (!channel || channel.length === 0) {
              return this.notFound('Channel doesn\'t exists!');
            }

            var workId = workflow.producer.addSingleWork(username, {
              headers: { operation: 'delchannel' },
              payload: {
                projectId: projectId,
                channelId: channelId
              }
            });

            return this.accepted(
              { channelId: channelId },
              this.urlFor('works', workId));
          }.bind(this),

          function onReject(error) {
            return this.notFound(error);
          }.bind(this)
        );
      })
    }),

    channelPackage: RESTApi({
      get: function(req, res, params) {
        return this.unavailable();
      }
    })
  };
};
