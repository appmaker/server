'use strict';

// GitHub protocol: https://developer.github.com/v3/oauth/

var log = require('AppMakerLogManager')('RESTAPI_V1_AUTH'),
    RESTApi = require('../restapi-tools').RESTApi,
    request = require('request'),
    getUUID = require('../common').getUUID,
    Promise = require('promise'),
    UrlUtils = require('url');

var post = Promise.denodeify(request.post);
var get = Promise.denodeify(request.get);

module.exports = function(libs) {
  var database = libs.database,
      ghConfig = libs.config.oauth2.github;

  ghConfig.login_uri = 'https://github.com/login/oauth/authorize';
  ghConfig.access_token_uri = 'https://github.com/login/oauth/access_token';

  return {
    auth: RESTApi({
      namespace: 'appmaker/v1',

      options: function () { return Promise.resolve(); },

      get: function (req, res, params, session) {
        var _this = this;
        if (params.get('provider') !== 'github') {
          return _this.notFound('Provider not supported');
        }

        var authId = getUUID();

        return session.getData()
          .then(createNewAuthentication)
          .then(sendToGitHub);

        function createNewAuthentication(sessionData) {
          var queryParams = JSON.parse(JSON.stringify(params.query));
          delete queryParams.redirectUrl;
          return database.authentications.set(authId, {
            sessionId: sessionData.sessionId,
            redirectUrl: params.get('redirectUrl'),
            query: queryParams
          });
        }

        function sendToGitHub() {
          var loginUri = ghConfig.login_uri +
                         '?client_id=' + ghConfig.client_id +
                         '&state=' + authId;
          return _this.redirect(loginUri);
        };
      }
    }),

    callback: RESTApi({
      namespace: 'appmaker/v1',

      options: function () { return Promise.resolve(); },

      get: function (req, res, params) {
        var _this = this;
        var targetUrl;

        if (params.get('provider') !== 'github') {
          return _this.notFound('Provider not supported');
        }

        return sendAuthorizationCode().then(processResponse, catchError);

        function catchError() {
          return _this.unavailable('authentication-sever-error');
        }

        function sendAuthorizationCode() {
          return post({
            url: ghConfig.access_token_uri,
            headers: {
              'Accept': 'application/json'
            },
            form: {
              client_id: ghConfig.client_id,
              client_secret: ghConfig.client_secret,
              code: params.get('code')
            }
          });
        }

        function processResponse(response) {
          log.debug('GitHub Response: ', response);

          var body;

          try {
            body = JSON.parse(response.body);
          }
          catch(error) {
            log.error('Error parsing github body:', error);
            return _this.unauthorized(error);
          }

          var accessToken = body.access_token;
          if (!accessToken) {
            var error = body.error_description || 'No access_token received';
            return _this.unauthorized(error);
          }
          else {
            var authId = params.get('state');

            if (!authId) {
              return _this.bad('invalid-state');
            }

            return getAuthentication()
              .then(extractSession)
              .then(getUserInfoFromGitHub)
              .then(saveUserInfo)
              .then(function () { return _this.redirect(targetUrl); })
              .catch(function (error) { return _this.bad(error); });
          }

          function getAuthentication() {
            return database.authentications.get(authId);
          }

          function extractSession(authData) {
            if (!authData) {
              return _this.bad('authentication-request-invalid');
            }
            targetUrl = buildTargetUrl(authData.redirectUrl, authData.query);
            return database.sessions.get(authData.sessionId);
          }

          function getUserInfoFromGitHub(sessionData) {
            return Promise.all([
              Promise.resolve(sessionData),
              get({
                url: 'https://api.github.com/user',
                headers: {
                  'Authorization': 'token ' + accessToken,
                  'User-Agent': 'appmaker'
                  // XXX: User-Agent header is mandatory for GitHub server
                }
              })
            ]);
          }

          function saveUserInfo(sessionDataAndResponse) {
            var sessionData = sessionDataAndResponse[0];
            var response = sessionDataAndResponse[1]
            log.debug('User Info: ', response);
            var body = JSON.parse(response.body);
            sessionData.username = body.login;
            sessionData.avatarUrl = body.avatar_url;
            sessionData.ghId = body.id;
            return database.sessions.set(
              sessionData.sessionId,
              sessionData
            );
          }

          function buildTargetUrl(base, params) {
            var url = UrlUtils.parse(base);
            url.query = params;
            return UrlUtils.format(url);
          }
        }
      }
    })
  }
};
