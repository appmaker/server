'use strict';

var log = require('AppMakerLogManager')('RESTAPI_V1_AUTH'),
    RESTApi = require('../restapi-tools').RESTApi,
    request = require('request'),
    getUUID = require('../common').getUUID,
    Promise = require('promise');

var post = Promise.denodeify(request.post);
var get = Promise.denodeify(request.get);

module.exports = function(libs) {

  return {
    profile: RESTApi({
      namespace: 'appmaker/v1',

      options: function () { return Promise.resolve(); },

      get: function (req, res, params, session) {
        var _this = this;

        return session.getData().then(checkIfAuthorized);

        function checkIfAuthorized(sessionData) {
          if (!sessionData || !sessionData.username) {
            return _this.unauthorized('no-logged-in');
          }
          return _this.ok({
            username: sessionData.username,
            avatarUrl: sessionData.avatarUrl
          });
        }
      },

      delete: function (req, res, params, session) {
        var _this = this;
        return session.setData({}).then(function () { return _this.ok(); });
      }
    })
  };
};
