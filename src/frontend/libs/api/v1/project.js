'use strict';

var getUUID = require('../common').getUUID,
    Promise = require('promise'),
    RESTApi = require('../restapi-tools').RESTApi,
    authenticated = require('../restapi-tools').decorators.authenticated;

module.exports = function(libs) {
  var database = libs.database,
      workflow = libs.workflow;

  var getProject =
          Promise.denodeify(database.projects.get.bind(database.projects)),
      getAllProjects =
          Promise.denodeify(database.projects.getAll.bind(database.projects)),
      getProjectByName = Promise.denodeify(
          database.projects.getProjectByName.bind(database.projects)
      );

  return {
    projects: RESTApi({
      namespace: 'appmaker/v1',

      get: authenticated(function (req, res, params, username) {
        return getAllProjects(username).then(function (data) {
          data = data || {};
          var projects = Object.keys(data).reduce(function (all, projectId) {
            var project = data[projectId];
            all.push(project);
            return all;
          }, []);
          return this.ok(projects);
        }.bind(this))
        .catch(function (error) {
          return this.unavailable(error);
        }.bind(this));
      }),

      post: authenticated(function (req, res, params, username) {
        var projectId = getUUID();
        var projectUrl = this.urlFor('projects', projectId);
        var payload = params.getAll();
        payload.projectId = projectId;
        delete payload.channels;

        return getProjectByName(username, params.get('name'))
        .then(function (project) {
          if (project) {
            return this.conflict('Project already exists');
          } else {
            var workId = workflow.producer.addSingleWork(username, {
              headers: { operation: 'newproject' },
              payload: payload,
              resourceUrl: projectUrl
            });

            return this.accepted(
              { projectId: projectId },
              this.urlFor('works', workId)
            );
          }
        }.bind(this));
      })
    }),

    oneProject: RESTApi({
      namespace: 'appmaker/v1',

      get: authenticated(function (req, res, params, username) {
        return getProject(username, params.get('projectId'))
        .then(
          function onFulfill(data) {
            if (data) {
              return this.ok(data);
            } else {
              return this.notFound('No project data stored');
            }
          }.bind(this),

          function onReject(error) {
            return this.notFound(error);
          }.bind(this)
        );
      }),

      put: authenticated(function (req, res, params, username) {
        var projectId = params.get('projectId');

        return getProjectByName(username, params.get('name'))
        .then(function (project) {
          if (!project || projectId === project.projectId) {
            return getProject(username, params.get('projectId'))
            .then(
              function (data) {
                if (!data) {
                  return this.notFound('No project data stored');
                }

                var workId = workflow.producer.addSingleWork(username, {
                  headers: { operation: 'updateproject' },
                  payload: {
                    projectId: projectId,
                    data: params.getAll()
                  },
                  resourceUrl: this.urlFor('projects', projectId)
                });
                return this.accepted(
                  { projectId: projectId },
                  this.urlFor('works', workId)
                );
              }.bind(this),

              function onReject(error) {
                return this.notFound(error);
              }.bind(this)
            );
          } else {
            return this.conflict('Project already exists');
          }

        }.bind(this));
      }),

      'delete': authenticated(function (req, res, params, username) {
        var projectId = params.get('projectId');

        return getProject(username, projectId)
        .then(function(projectData) {
          if (!projectData) {
            return this.notFound('Missing project');
          }

          var metaworkId = workflow.producer.getNewWorkId();
          var allDeleted =
            projectData.channels.reduce(function (added, channel) {
              return added.then(function () {
                var channelDropWorkInfo = workflow.producer.addWork(username, {
                  headers: { operation: 'delchannel' },
                  payload: {
                    projectId: projectId,
                    channelId: channel.channelId
                  }
                }, metaworkId);
                return channelDropWorkInfo.onceAdded;
              });
            }, Promise.resolve());
          allDeleted.then(function() {
            var projectDropWorkInfo = workflow.producer.addWork(username, {
              headers: { operation: 'delproject' },
              payload: {
                projectId: params.get('projectId')
              }
            }, metaworkId);
            projectDropWorkInfo.onceAdded.then(function() {
              workflow.producer.dispatch(metaworkId);
            });
          })
          .catch(function() {
            console.log('Error processing delete project workflows');
          });

          return this.accepted(
            { projectId: projectId },
            this.urlFor('works', metaworkId)
          );
        }.bind(this),

        function onGetProjectReject(error) {
          return this.notFound(error);
        }.bind(this));
      })
    })
  };
};
