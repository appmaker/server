'use strict';

var check_service_status = require('../../check_service_status'),
    Promise = require('promise'),
    RESTApi = require('../restapi-tools').RESTApi;

check_service_status = Promise.denodeify(check_service_status);

module.exports = function(libs) {
  return {
    getWorkIdStatus: RESTApi(function(req, res, params) {
      var self = this;
      var getStatus = Promise.denodeify(libs.database.works.getStatus.bind(libs.database.works));
      return getStatus(params.get('workId'))
      .then(function onFullfil(data) {
        // Received workId is a single subwork
        data.workId = params.get('workId');
        var isComplete = data.progress === 100;
        return self.ok(data, isComplete ? data.resourceUrl : undefined);
      }, function onReject(error) {
        var getWorkflow = Promise.denodeify(libs.database.works.getWorkflow.bind(libs.database.works));
        return getWorkflow(params.get('workId'))
        .then(function onFullfil(data) {
          // Received workId is a metawork with multiple subworks
          if (data.nextWork === null) {
            return self.notFound(error);
          } else if (data.nextWork === 0) {
            return self.ok({
              progress: 0,
              workId: params.get('workId')
            });
          }

          // Calculate metawork status
          data.subWorks = data.subWorks.map(function(work) {
            return work.id
          });
          var subWorksPromises = [];
          for (var i = 0; i < data.nextWork; i++) {
            subWorksPromises.push(getStatus(data.subWorks[i]));
          }

          return Promise.all(subWorksPromises).then(function (worksData) {
            data.progress = worksData.reduce(
              function (currentProgress, workData) {
                // TODO: Workaround ISSUE #30
                //  workData.progress is an array
                var workProgress = Array.isArray(workData.progress) ?
                  workData.progress[0] : workData.progress;

                return currentProgress + workProgress;
              }, 0) / data.subWorks.length;

            delete data.nextWork;
            // TODO: Resource URL should be at metawork level
            data.resourceUrl = worksData[0].resourceUrl;
            data.workId = params.get('workId');
            var isComplete = data.progress === 100;
            if (isComplete) {
              data.status = 0; // TODO: this should be recovered, 0 means OK
            }
            return self.ok(data, isComplete ? data.resourceUrl : undefined);
          }, function (error) {
            return self.notFound(error);
          });
        }, function onReject(error) {
          return self.notFound(error);
        });
      });
    }),

    getOriginTypes: RESTApi(function(req, res, params) {
      var self = this;
      var getOriginTypes = Promise.denodeify(libs.database.general.getOriginTypes.bind(libs.database.general));
      return getOriginTypes()
      .then(function (types) {
        var originTypes = Object.keys(types).map(function (type) {
          types[type].originTypeId = type;
          return types[type];
        }, []);
        return self.ok(originTypes);
      })
      .catch(function (error) {
        return self.unavailable(error);
      });
    }),

    getDeploymentTypes: RESTApi(function(req, res, params) {
      var self = this;
      var getDeploymentTypes = Promise.denodeify(libs.database.general.getDeploymentTypes.bind(libs.database.general));
      return getDeploymentTypes()
      .then(function(types) {
        var deploymentTypes = Object.keys(types).map(function (type) {
          types[type].deploymentTypeId = type;
          return types[type];
        }, []);
        return self.ok(deploymentTypes);
      })
      .catch(function () {
        return self.unavailable();
      });
    }),

    getPermissions: RESTApi(function(req, res, params) {
      var self = this;
      var getPermissions = Promise.denodeify(libs.database.general.getPermissions.bind(libs.database.general));
      return getPermissions()
      .then(function (permissions) {
        return self.ok(permissions);
      })
      .catch(function (error) {
        return self.unavailable(error);
      });
    }),

    getPlatforms: RESTApi(function(req, res, params) {
      var self = this;
      var getPlatforms = Promise.denodeify(libs.database.general.getPlatforms.bind(libs.database.general));
      return getPlatforms()
      .then(function (platforms) {
        return self.ok(platforms);
      })
      .catch(function (error) {
        return self.unavailable(error);
      });
    }),

    getServerStatus: RESTApi(function(req, res, params) {
      // XXX: can not fail
      return check_service_status().then(this.ok.bind(this));
    })
  };
};
