'use strict'

var cookie = require('cookie'),
    getUUID = require('./common').getUUID,
    sessionName = 'appmaker_session',
    Promise = require('promise');

module.exports = function ensureSession(req, res) {
  var cookieHeader = req.headers.cookie || '';
  var sessionId = cookie.parse(cookieHeader)[sessionName];

  if (!sessionId) {
    return newSession();
  }

  var session = new Session(sessionId);
  return session.exists().then(function (exists) {
    return exists ? session : newSession();
  });

  function newSession() {
    var sessionId = getUUID();
    res.setHeader('Set-Cookie', cookie.serialize(sessionName, sessionId));
    var session = new Session(sessionId);
    return session.setData({}).then(function () {
      return session;
    });
  }
};

function Session(sessionId) {
  Object.defineProperty(this, 'id', { value: sessionId  });
}

Session.prototype.getData = function () {
  return global.database.sessions.get(this.id);
};

Session.prototype.setData = function (data) {
  data = data || {};
  data.sessionId = this.id;
  return global.database.sessions.set(this.id, data);
};

Session.prototype.exists = function () {
  return this.getData().then(function (sessionData) {
    return sessionData && sessionData.sessionId;
  });
};
