'use strict';

function debugOutput(error, object, res, cb) {
  if (error) {
    cb({
      code: 503,
      error: error
    });
  } else {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<pre>' + JSON.stringify(object, null, '  ') + '</pre>');
    cb();
  }
}

module.exports = function(libs) {
  return {
    getPlatforms: function(req, res, params, cb) {
      libs.database.general.getPlatforms(function(e, d) {
        debugOutput(e, d, res, cb);
      });
    },

    getPermissions: function(req, res, params, cb) {
      libs.database.general.getPermissions(function(e, d) {
        debugOutput(e, d, res, cb);
      });
    },

    getWorkflows: function(req, res, params, cb) {
      libs.database.general.getWorkflows(function(e, d) {
        debugOutput(e, d, res, cb);
      });
    },

    getWorkflowsInRAM: function(req, res, params, cb) {
      debugOutput(null, libs.workflow.debug, res, cb);
    }
  };
};
