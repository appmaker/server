'use strict';

var crypto = require('crypto'),
    uuid = require('uuid').v4,
    fs = require('fs');

function getHash(data) {
  return crypto.createHash('sha1').update(data).digest('hex');
}

module.exports = {
  getHash: getHash,

  fileHash: function(filePath, cb) {
    fs.readFile(filePath, function(err, data) {
      var hash = null;
      if (!err) {
        hash = getHash(data);
      }
      cb(null, hash);
    });
  },

  getUUID: uuid
};
