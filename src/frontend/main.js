'use strict';

var rest_server = require('restapi').server,
    log = require('AppMakerLogManager')('AppMakerFrontend'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager');
require('AppMakerProcessManager')('AppMakerFrontend');

var serverRunning = false,
    api = {};

log.debug('Starting server...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    global.database = database;
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startService(cfg.frontend);
    });
  });
});

function startService(cfg) {
  function loadAPIs() {
    var libs = {
      database: database,
      workflow: workflow,
      config: cfg
    };
    api = {
      v1: require('./libs/api/v1')(libs),
      debug: require('./libs/api/debug')(libs)
    };
  }

  if (serverRunning) {
    log.debug('Server is running, no need to start it again');
    return
  }
  log.debug('Starting service...');
  serverRunning = true;

  var port = cfg.port;
  if (process.argv.length > 2 &&
      parseInt(process.argv[2]) > 0 && parseInt(process.argv[2]) < 65535) {
    port = parseInt(process.argv[2]);
    log.debug('Getting port from arguments: ' + port);
  } else if (process.env['APPMAKER_PORT'] &&
             parseInt(process.env['APPMAKER_PORT']) > 0 &&
             parseInt(process.env['APPMAKER_PORT']) < 65535) {
    port = parseInt(process.env['APPMAKER_PORT']);
    log.debug('Getting port from environment: ' + port);
  } else {
    log.debug('Using default listening port: ' + port);
  }

  log.debug('Loading REST API...');
  loadAPIs();
  var server = new rest_server('./appmaker.rdf', {
    port: port,
    debug: process.env['RESTAPI_DEBUG'] ? true : false,
    log: {
      info: require('AppMakerLogManager')('REST API').info,
      debug: process.env['RESTAPI_DEBUG'] ?
             require('AppMakerLogManager')('REST API').debug : function() {}
    }
  });

  /////////////////////////////////////////////////////////////////////////////
  // v1
  /////////////////////////////////////////////////////////////////////////////

  // Main resources
  var profile = api.v1.profile;
  server['appmaker/v1/profile'] = profile.profile;

  var auth = api.v1.auth;
  server['appmaker/v1/auth/:provider'] = auth.auth;
  server['appmaker/v1/auth/:provider/callback'] = auth.callback;

  var origin = api.v1.origin;
  server['appmaker/v1/origins'] = origin.origins;
  server['GET appmaker/v1/origins/:originId'] = origin.oneOrigin;
  server['GET appmaker/v1/origins/:originId/source'] = origin.oneOriginPackage;

  var project = api.v1.project;
  server['appmaker/v1/projects'] = project.projects;
  server['appmaker/v1/projects/:projectId'] = project.oneProject;

  var channel = api.v1.channel;
  server['appmaker/v1/projects/:projectId/channels'] = channel.channels;
  server['appmaker/v1/projects/:projectId/channels/:channelId'] =
    channel.oneChannel;
  server['appmaker/v1/projects/:projectId/channels/:channelId/package'] =
    channel.channelPackage;

  // Informative methods
  server['GET appmaker/v1/works/:workId'] = api.v1.info.getWorkIdStatus;
  server['GET appmaker/v1/origin-types'] = api.v1.info.getOriginTypes;
  server['GET appmaker/v1/deployment-types'] = api.v1.info.getDeploymentTypes;
  server['GET appmaker/v1/permissions'] = api.v1.info.getPermissions;
  server['GET appmaker/v1/platforms'] = api.v1.info.getPlatforms;
  server['GET appmaker/v1/status'] = api.v1.info.getServerStatus;

  /////////////////////////////////////////////////////////////////////////////
  // debug
  /////////////////////////////////////////////////////////////////////////////

  server['GET appmaker/debug/platforms'] = api.debug.getPlatforms;
  server['GET appmaker/debug/permissions'] = api.debug.getPermissions;
  server['GET appmaker/debug/workflows'] = api.debug.getWorkflows;
  server['GET appmaker/debug/workflowsram'] = api.debug.getWorkflowsInRAM;

  /////////////////////////////////////////////////////////////////////////////
  // Any other method pending to be implemented will go to this endpoint
  /////////////////////////////////////////////////////////////////////////////

  server.onendpoint = function(req, res, endpoint, params, method, cb) {
    log.debug(endpoint);
    log.debug(method);
    if (method === 'OPTIONS') {
      cb();
      return;
    }
    cb({
      code: 501,
      message: 'Not implemented'
    });
  };
}
