'use strict';

var log = require('AppMakerLogManager')('DeployWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    Promise = require('promise'),
    path = require('path'),
    fs = require('fs-extra');
require('AppMakerProcessManager')('DeployWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker(cfg);
    });
  });
});

var getChannel =
  Promise.denodeify(database.projects.getChannel.bind(database.projects));

function existsFolder(path) {
  return new Promise(function (fulfill) {
    fs.exists(path, fulfill);
  });
};

function createFolder(folder) {
  var fsMkdirp = Promise.denodeify(fs.mkdirp);
  return existsFolder(folder).then(function (exists) {
    return exists ? Promise.resolve() : fsMkdirp(folder);
  });
}

function deleteFolder(folder) {
  var fsRmrf = Promise.denodeify(fs.remove);
  return existsFolder(folder).then(function (exists) {
    return exists ? fsRmrf(folder) : Promise.resolve();
  });
}

function startWorker(cfg) {
  workflow.workers.connectWorker('deployworker:ssh',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("DEPLOY SSH:" + JSON.stringify(message));
      log.debug("DEPLOY SSH:" + JSON.stringify(headers));
      log.debug("DEPLOY SSH:" + JSON.stringify(deliveryInfo));

      next();
    });

  workflow.workers.connectWorker('deployworker:github',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("DEPLOY GITHUB:" + JSON.stringify(message));
      log.debug("DEPLOY GITHUB:" + JSON.stringify(headers));
      log.debug("DEPLOY GITHUB:" + JSON.stringify(deliveryInfo));

      next();
    });

  workflow.workers.connectWorker('deployworker:appmaker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("DEPLOY APPMAKER:" + JSON.stringify(message));
      log.debug("DEPLOY APPMAKER:" + JSON.stringify(headers));
      log.debug("DEPLOY APPMAKER:" + JSON.stringify(deliveryInfo));

      var targetPath = path.join(cfg.appworker.paths.hostedApps,
        message.data.deployment.params.subdomain);
      log.debug("targetPath: " + targetPath);
      deleteFolder(targetPath)
      .then(function() {
        return createFolder(targetPath);
      })
      .then(function() {
        log.info('Folder created ' + targetPath);

        database.projects.get(headers.user, message.projectId,
          function(e, projectData) {
            if (e) {
              log.error('No valid data found ! - ' + e);
              next();
              return;
            }

            var channelData = [];
            if (projectData && projectData.channels) {
              channelData = projectData.channels.filter(function(channel) {
                return channel.channelId === message.channelId;
              });
            }
            if (channelData.length === 0) {
              log.error('No valid channel found !');
              next();
              return;
            }
            channelData = channelData[0];

            var originPath = path.join(channelData.path, 'output');

            fs.copy(originPath, targetPath, next);
          });
      })
      .catch(function(e) {
        log.error('Error ' + e);
        next();
      });
    });

  workflow.workers.connectWorker('deployworker:delappmaker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("UN-DEPLOY APPMAKER:" + JSON.stringify(message));
      log.debug("UN-DEPLOY APPMAKER:" + JSON.stringify(headers));
      log.debug("UN-DEPLOY APPMAKER:" + JSON.stringify(deliveryInfo));

      getChannel(headers.user, message.projectId, message.channelId)
      .then(function(channel) {
        var targetPath = path.join(cfg.appworker.paths.hostedApps,
          channel.deployment.params.subdomain);
        log.debug("targetPath to remove: " + targetPath);
        deleteFolder(targetPath).then(next);
      })
      .catch(function(error) {
        log.error('UN-DEPLOY Error: ' + error);
        next();
      });
    });

  log.debug('DeployWorker started !');
}
