'use strict';

var log = require('AppMakerLogManager')('OriginWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    path = require('path'),
    fs = require('fs-extra'),
    Promise = require('promise');
require('AppMakerProcessManager')('OriginWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker(cfg.appworker.paths.targetOriginSubfolder);
    });
  });
});

var fsRemove = Promise.denodeify(fs.remove),
    fsMkdir = Promise.denodeify(fs.mkdirp),
    fsCopy = Promise.denodeify(fs.copy);

function startWorker(targetOriginSubfolder) {
  workflow.workers.connectWorker('originworker:checkvirus',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ORIGIN-CheckVirus: " + JSON.stringify(message));
      log.debug("ORIGIN-CheckVirus: " + JSON.stringify(headers));
      log.debug("ORIGIN-CheckVirus: " + JSON.stringify(deliveryInfo));

      var originData = message;
      originData.virus = "clean";
      database.origins.update(originData, next);
    });

  workflow.workers.connectWorker('originworker:checkmanifest',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ORIGIN-CheckManifest: " + JSON.stringify(message));
      log.debug("ORIGIN-CheckManifest: " + JSON.stringify(headers));
      log.debug("ORIGIN-CheckManifest: " + JSON.stringify(deliveryInfo));

      var manifestFile = path.join(message.path, targetOriginSubfolder,
        'manifest.webapp');
      fs.exists(manifestFile, function(exists) {
        if (exists) {
          fs.readFile(manifestFile, {
            encoding: 'utf-8'
          }, function(err, data) {
            if (err) {
              log.error('Error reading manifest file');
              next();
            } else {
              try {
                var originData = message;
                originData.manifest = JSON.parse(data);
                database.origins.update(originData, next);
              } catch(e) {
                log.error('Error analysing manifest file');
                next();
              }
            }
          });
        } else {
          log.info('No manifest file found');
          next();
        }
      });
    });

  workflow.workers.connectWorker('originworker:getcontents',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ORIGIN-GetContents: " + JSON.stringify(message));
      log.debug("ORIGIN-GetContents: " + JSON.stringify(headers));
      log.debug("ORIGIN-GetContents: " + JSON.stringify(deliveryInfo));

      var actionByType = {
        'zip': function(originData, channelData) {
          var sourcePath = path.join(originData.path, originData.src.name),
              targetPath = channelData.path;

          fsRemove(targetPath).then(function() {
            return fsMkdir(targetPath);
          })
          .then(function() {
            targetPath = path.join(targetPath, originData.src.name);
            return fsCopy(sourcePath, targetPath);
          })
          .then(function() {
            sourcePath = path.join(originData.path,
              targetOriginSubfolder);
            targetPath = path.join(channelData.path, 'input');
            return fsCopy(sourcePath, targetPath);
          })
          .then(function() {
            targetPath = path.join(channelData.path, 'output');
            return fsCopy(sourcePath, targetPath);
          })
          .then(function() {
            message.targetPath = targetPath;
            next();
          })
          .catch(function(error) {
            log.error('Error getting contents', error);
            // TODO: Inform errors to the workflow manager (OWDAPPMAKE-688)
            next();
          });
        }
      };

      var originId = message.data.originId;
      database.origins.get(originId, function(e, originData) {
        if (e) {
          log.error('No origin found ! - ' + e);
          next();
          return;
        }

        database.projects.get(headers.user, message.projectId,
          function(e, projectData) {
            if (e) {
              log.error('No valid data found ! - ' + e);
              next();
              return;
            }

            var channelData = [];
            if (projectData && projectData.channels) {
              channelData = projectData.channels.filter(function(channel) {
                return channel.channelId === message.channelId;
              });
            }
            if (channelData.length === 0) {
              log.error('No valid channel found !');
              next();
              return;
            }
            channelData = channelData[0];
           if (typeof actionByType[originData.type] === 'function') {
              actionByType[originData.type](originData, channelData);
            }
          });
      });
    });

  log.debug('OriginWorker started !');
}
