'use strict';

var im = require('imagemagick'),
    path = require('path'),
    Promise = require('promise');
var imConvert = Promise.denodeify(im.convert);

function createIcons(image, dir) {
  var files = [
    { filename: 'touch-icon-iphone.png', size: 57 },
    { filename: 'touch-icon-iphone-retina.png', size: 114 },
    { filename: 'touch-icon-ipad-retina.png', size: 144 },
    { filename: 'tileicon.png', size: 144 },
    { filename: 'favicon.png', size: 96 },
    { filename: 'icon_60.png', size: 60 },
    { filename: 'icon_128.png', size: 128 }
  ];
  var conversions = files.map(function (spec) {
    var size = spec.size,
        target = path.join(dir, spec.filename);
    return imConvert([image, '-resize', size + 'x' + size, target]);
  });
  return Promise.all(conversions);
}

module.exports = createIcons;
