'use strict';

var log = require('AppMakerLogManager')('IconsWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    iconGenerator = require('./lib/icon/index.js'),
    Promise = require('promise'),
    path = require('path'),
    fs = require('fs');
require('AppMakerProcessManager')('IconsWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('iconsworker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ICON: " + JSON.stringify(message));
      log.debug("ICON: " + JSON.stringify(headers));
      log.debug("ICON: " + JSON.stringify(deliveryInfo));

      if (!message.targetPath) {
        log.error('Bad paramateres, no targetPath received!');
        return next();
      }

      var originId = message.data.appInfo.iconOrigin;
      if (!originId) {
        log.error('Bad paramateres, no originId received!');
        return next();
      }

      var getOrigin =
        Promise.denodeify(database.origins.get.bind(database.origins));

      getOrigin(originId)
      .then(function (origin) {
        var iconPath = path.join(origin.path, origin.src.name);
        var targetPath = path.join(message.targetPath, 'icons');
        if (!fs.existsSync(targetPath)) {
          fs.mkdirSync(targetPath);
        }

        return iconGenerator(iconPath, targetPath)
          .then(function () { next(); })
          .catch(function () { next(); });

      })
      .catch(function () { next(); });
    });

  log.debug('IconsWorker started !');
}
