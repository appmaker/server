'use strict';

var log = require('AppMakerLogManager')('MutateDOMWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    mutator = require('./lib/dommutator');
require('AppMakerProcessManager')('MutateDOMWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('mutatedomworker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("MutateDOMWorker: " + JSON.stringify(message));
      log.debug("MutateDOMWorker: " + JSON.stringify(headers));
      log.debug("MutateDOMWorker: " + JSON.stringify(deliveryInfo));

      mutator.mutateDom({
        output: message.targetPath
      }, next);
    });

  log.debug('MutateDOMWorker started !');
}
