'use strict';

require('colors');
var fs = require('fs');
var cheerio = require('cheerio');

var DomMutator = function DomMutator() {

  // Modify the DOM to add all the goodies from
  // Cacheator.
  // By default searchs index.html in the output
  // folder
  function mutateDom(config, cb) {
    var index = config.output + '/index.html';
    var appCacheVersion = index;
    var appSWVersion = config.output + '/index_sw.html';

    fs.exists(index, function (exists) {
      if (!exists) {
        console.log('Could not find'.yellow, index.yellow);
        if (typeof cb === 'function') {
          cb();
        }
        return;
      }
      var appCacheMutator = cheerio.load(fs.readFileSync(index));
      var swMutator = cheerio.load(fs.readFileSync(index));
      generateAppCacheVersion(appCacheMutator, appCacheVersion, sync);
      // XXX: Disabling until recovering service worker functionality
      // Remember setting remainingTasks to 2 when re-enabling
      //generateSWVersion(swMutator, appSWVersion, sync);
    });


    var errors;
    var remainingTasks = 1;
    function sync(error) {
      if (error) {
        errors = errors || [];
        errors.push(error);
      }
      remainingTasks--;
      if (!remainingTasks) cb(errors);
    }
  }

  function generateAppCacheVersion($, out, cb) {
    ensureHead($);
    addUpdateAppCache($);
    addManifest($);
    addIcons($);
    add2Home($);

    fs.writeFile(out,
      $.html(), function(err) {
      if (err) {
        throw err;
      }

      if (typeof cb === 'function') {
        cb();
      }
    });
  }

  function generateSWVersion($, out, cb) {
    ensureHead($);
    addSW($);
    addIcons($);
    add2Home($);

    fs.writeFile(out,
      $.html(), function(err) {
      if (err) {
        throw err;
      }

      if (typeof cb === 'function') {
        cb();
      }
    });
  }

  function ensureHead($) {
    var $head = $('head');
    if (!$head.length) { $('html').prepend('<head>'); }
  }


  function  addUpdateAppCache($) {
    $('head').prepend('<script src="./_update_appcache.js"></script>');
  }

  function addManifest($) {
    $('html').attr('manifest', 'manifest.appcache');
  }

  function addSW($) {
    $('html').removeAttr('manifest');
    $('head').prepend('<script src="./offline-cache-setup.js"></script>');
  }

  function addIcons($) {
    // All iOS versions
    $('head').append('<link rel="apple-touch-icon" href="icons/touch-icon-iphone.png">\n');
    $('head').append('<link rel="apple-touch-icon" sizes="120x120" href="icons/touch-icon-iphone-retina.png">\n');
    $('head').append('<link rel="apple-touch-icon" sizes="76x76" href="icons/favicon.png">\n');
    $('head').append('<link rel="apple-touch-icon" sizes="152x152" href="icons/touch-icon-ipad-retina.png">\n');
    // Normal favicon
    $('head').append('<link rel="icon" href="icons/favicon.png">\n');
    // Windows 7 tile
    $('head').append('<meta name="msapplication-TileImage" content="icons/tileicon.png">\n');
  }

  function add2Home($) {
    $('head').append('<script type="text/javascript" src="add2home/js/add2home.js"></script>\n');
    $('head').append('<link href="add2home/css/add2home.css" rel="stylesheet">\n');
  }

  return {
    'mutateDom': mutateDom
  };

}();

module.exports = DomMutator;
