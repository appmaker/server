NPM ?= npm
NODE ?= node

all: config.default.json dependencies

node_modules:
	@echo "Installing node dependencies..."
	@$(NPM) update
	@echo "Done!"

dependencies: node_modules config.default.json
	@echo "Installing common dependencies..."
	@$(NPM) install ../../common/loggerManager
	@$(NPM) install ../../common/configManager
	@$(NPM) install ../../common/databaseManager
	@$(NPM) install ../../common/workflowManager
	@$(NPM) install ../../common/processManager
	@echo "Done!"

config.default.json: ../../../cfg/defaultConfig.json
	@echo "Copying default config file..."
	@cp ../../../cfg/defaultConfig.json config.default.json

clean:
	@rm -f config.default.json
	@rm -f npm-debug.log
	@rm -rf node_modules

run: dependencies 
	@$(NODE) index.js &
