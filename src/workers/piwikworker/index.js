'use strict';

var log = require('AppMakerLogManager')('PiwikWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager');
require('AppMakerProcessManager')('PiwikWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('piwikworker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("PIWIK: " + JSON.stringify(message));
      log.debug("PIWIK: " + JSON.stringify(headers));
      log.debug("PIWIK: " + JSON.stringify(deliveryInfo));

      next();
    });

  log.debug('PiwikWorker started !');
}
