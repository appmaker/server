'use strict';

var log = require('AppMakerLogManager')('GitWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager');
require('AppMakerProcessManager')('GitWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('gitworker:clone',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("GIT: " + JSON.stringify(message));
      log.debug("GIT: " + JSON.stringify(headers));
      log.debug("GIT: " + JSON.stringify(deliveryInfo));

      next();
    });

  log.debug('GitWorker started !');
}
