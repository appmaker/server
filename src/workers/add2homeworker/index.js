'use strict';

var log = require('AppMakerLogManager')('Add2HomeWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    add2home = require('./lib/add2home');
var _cfg = null;
require('AppMakerProcessManager')('Add2HomeWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  _cfg = cfg;
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('add2homeworker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ADD2HOME: " + JSON.stringify(message));
      log.debug("ADD2HOME: " + JSON.stringify(headers));
      log.debug("ADD2HOME: " + JSON.stringify(deliveryInfo));

      // TODO: Add2Home configuration
      database.projects.get(headers.user,
        message.projectId, function(error, projectData) {
          if (error) {
            log.error("No valid project - " + error + " - " + message.projectId);
            next();
            return;
          }

          var channelData = [];
          if (projectData && projectData.channels) {
            channelData = projectData.channels.filter(function(channel) {
              return channel.channelId === message.channelId;
            });
          }
          if (channelData.length === 0) {
            log.error('No valid channel found !');
            next();
            return;
          }
          channelData = channelData[0];

          add2home.populate({
            output: message.targetPath,
            manifest: {
              name: channelData.appInfo.name,
              description: channelData.appInfo.description || '',
              icon: 'noicon.png'
            },
            subdomain: channelData.deployment.params.subdomain,
            protocol: _cfg.publicUrl.protocol,
            masterDomain: _cfg.publicUrl.host,
            publicPort: _cfg.publicUrl.port
          }, next);
      });
    });

  log.debug('Add2HomeWorker started !');
}
