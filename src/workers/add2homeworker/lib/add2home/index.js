'use strict';

var fs = require('fs-extra');

var Add2Home = function Add2Home() {

  var params = {
    "name" : "%APP_NAME%",
    "description" : "%APP_DESC%",
    "version" : "%APP_VERSION%",
    "url" : "%LAUNCHPATH%",
    "baseUrl" : "%BASE_URL%",
    "subdomainUrl" : "",
    "fileUrl" : "%FILE_URL%",
    "chromeItem": "%CHROME_STORE_ID%",
    "icon128" : "%ICON_128%"
  };

  function cleanAdd2Home(resourcesDir, cb) {
    fs.exists(resourcesDir, function(exists) {
      if (exists) {
        fs.remove(resourcesDir, cb);
      } else {
        cb();
      }
    });
  }

  // Given the config, witht he output dir
  // moves resources and generates files necessary
  // for add2home
  var populate = function populate(config, cb) {
    // Copy resources
    var resourcesDir = config.output + '/add2home/';
    cleanAdd2Home(resourcesDir, function(err) {
      if (err) {
        throw err;
      }

      fs.copy(__dirname + '/resources/', resourcesDir, function(err) {
        if (err) {
          throw err;
        }

        // Generate config json for extra add2home parameters
        generateJSON(config, cb);
      });
    });
  };

  var generateJSON = function generateJSON(config, cb) {
    Object.keys(params).forEach(function(key) {
      params[key] = config.manifest[key] || '';
    });

    // Simple params ovewriting
    params.baseUrl = getBaseUrl(config);

    fs.writeFile(config.output + '/app.json',
      JSON.stringify([params]), function(err) {
      if (err) {
        throw err;
      }

      if (typeof cb === 'function') {
        cb();
      }
    });
  };

  function getBaseUrl(config) {
    if (config.masterDomain) {
      return config.protocol + '://' + config.subdomain + '.' +
        config.masterDomain + ':' + config.publicPort;
    } else {
      return 'http://' + config.subdomain + '.localhost:3000';
    }
  }

  return {
    'populate': populate
  };

}();

module.exports = Add2Home;
