'use strict';

var log = require('AppMakerLogManager')('ManifestWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    manifest = require('./lib/manifest');
require('AppMakerProcessManager')('ManifestWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('manifestworker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("MANIFEST: " + JSON.stringify(message));
      log.debug("MANIFEST: " + JSON.stringify(headers));
      log.debug("MANIFEST: " + JSON.stringify(deliveryInfo));

      database.projects.get(headers.user, message.projectId,
        function(e, projectData) {
          if (e) {
            log.error('No valid data found ! - ' + e);
            next();
            return;
          }

          var channelData = [];
          if (projectData && projectData.channels) {
            channelData = projectData.channels.filter(function(channel) {
              return channel.channelId === message.channelId;
            });
          }
          if (channelData.length === 0) {
            log.error('No valid channel found !');
            next();
            return;
          }
          channelData = channelData[0];

          // TODO: manifest config
          manifest.generateManifests({
            output:  message.targetPath,
            manifest: {
              name: channelData.appInfo.name,
              version: channelData.appInfo.version || channelData.tag,
              description: channelData.appInfo.description
            }
          }, next);
        });
      });

  log.debug('ManifestWorker started !');
}
