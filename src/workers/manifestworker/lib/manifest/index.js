'use strict';

var fs = require('fs');

var Manifest = function Manifest() {

  var generate = function(config, cb) {
    generateFFOS(config, function() {
      generateChrome(config, cb);
    });
  }

  function generateFFOS(config, cb) {
    var manifestFile = config.output + '/manifest.webapp';
    var content = config.manifest;
    content.launch_path = '/';
    content.icons = {
      '60': '/icons/icon_60.png',
      '128': '/icons/icon_128.png'
    };
    content.appcache_path = '/manifest.appcache';
    content.permissions = {
      'geolocation': { 'description': 'Required to get your location'},
      'storage': { 'description': 'To store information in your device'},
      'alarms': { 'description': 'To alert you'},
      'audio-channel-normal': { 'description': 'To play sound'},
      'audio-channel-content': { 'description': 'To play sound'},
      'desktop-notification': { 'description': 'To notify you'},
      'fmradio': { 'description': 'To listen to your fm radio'}
    };

    // Remove old key for icon
    delete content.icon;

    fs.writeFile(manifestFile,
      JSON.stringify(content), function(err) {
      if (err) {
        throw err;
      }

      if (typeof cb === 'function') {
        cb();
      }
    });
  };

  function generateChrome(config, cb) {
    var manifestFile = config.output + '/manifest.json';
    var content = {
      'manifest_version': 2,
      'name': config.manifest.name,
      'version': config.manifest.version,
      'minimun_chrome_version': '31',
      'app': {
        'urls': ['/'],
        'launch': {
          'web_url' : '/index.html'
        }
      },
      'offline_enabled': true,
      'icons': {'128': '/icons/icon_128.png'}
    };
    
    fs.writeFile(manifestFile,
      JSON.stringify(content), function(err) {
      if (err) {
        throw err;
      }

      if (typeof cb === 'function') {
        cb();
      }
    });
  };

  return {
    'generateManifests': generate
  }

}();

module.exports = Manifest;
