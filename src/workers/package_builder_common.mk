PACKAGESKELFOLDER ?= ../../../packages
DISTFOLDER ?= ../../../dist
PACKAGENAME ?= appmaker-$(WORKERNAME)worker
VERSION ?= 0.0.1

cleandist:
	@rm -rf $(DISTFOLDER)/$(WORKERNAME)
	@rm -rf $(DISTFOLDER)/debs/$(PACKAGENAME)
	@rm -rf $(DISTFOLDER)/debs/$(PACKAGENAME)*.deb

dist: cleandist all
	@mkdir -p $(DISTFOLDER)/$(WORKERNAME)/
	@cp -al * $(DISTFOLDER)/$(WORKERNAME)
	@rm $(DISTFOLDER)/$(WORKERNAME)/Makefile

debian: dist
	@mkdir -p $(DISTFOLDER)/debs
	@cp -a $(PACKAGESKELFOLDER)/debian/workers.skel $(DISTFOLDER)/debs/$(PACKAGENAME)
	@find $(DISTFOLDER)/debs/$(PACKAGENAME) -type f -exec sed -i 's/%WORKERNAME%/$(WORKERNAME)/g' {} \;
	@mv $(DISTFOLDER)/debs/$(PACKAGENAME)/lib/systemd/system/appmaker_worker_WORKERNAME.service $(DISTFOLDER)/debs/$(PACKAGENAME)/lib/systemd/system/appmaker_worker_$(WORKERNAME).service
	@mkdir -p $(DISTFOLDER)/debs/$(PACKAGENAME)/usr/bin/appmaker/workers
	@cp -al $(DISTFOLDER)/$(WORKERNAME) $(DISTFOLDER)/debs/$(PACKAGENAME)/usr/bin/appmaker/workers
	@cd $(DISTFOLDER)/debs && dpkg --build $(PACKAGENAME) $(PACKAGENAME)_$(VERSION).deb
