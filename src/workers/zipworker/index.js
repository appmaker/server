'use strict';

var log = require('AppMakerLogManager')('ZipWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    path = require('path'),
    zip = require('adm-zip');
require('AppMakerProcessManager')('ZipWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker(cfg.appworker.paths.targetOriginSubfolder);
    });
  });
});

function startWorker(targetOriginSubfolder) {
  workflow.workers.connectWorker('zipworker:unzip',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ZIP: " + JSON.stringify(message));
      log.debug("ZIP: " + JSON.stringify(headers));
      log.debug("ZIP: " + JSON.stringify(deliveryInfo));

      var z = zip(path.join(message.path, message.src.name));
      message.src.path = path.join(message.path, targetOriginSubfolder);
      z.extractAllTo(message.src.path);
      database.origins.update(message, next);
    });

  log.debug('ZipWorker started !');
}
