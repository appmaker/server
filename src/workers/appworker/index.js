'use strict';

var log = require('AppMakerLogManager')('AppWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    path = require('path'),
    fs = require('fs-extra');
require('AppMakerProcessManager')('AppWorker');

var mainFolder = '/tmp';

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  mainFolder = cfg.appworker.paths.workingDir;
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      init(startWorker);
    });
  });
});

function createFolder(folderName, cb) {
  fs.exists(folderName, function(exists) {
    if(exists) {
      log.debug(folderName + ' already exists');
      cb();
    } else {
      log.info('Creating ' + folderName + ' folder...');
      fs.mkdirp(folderName, cb);
    }
  });
}

function deleteFolder(folderName, cb) {
  fs.exists(folderName, function(exists) {
    if(exists) {
      fs.remove(folderName, cb);
    } else {
      cb();
    }
  });
}

function init(cb) {
  log.debug('Checking hostedApps folder');
  createFolder(mainFolder, cb);
}

function startWorker() {
  workflow.workers.connectWorker('appworker:neworigin',
    function onNewOrigin(message, headers, deliveryInfo, messageObject, next) {
      log.debug("ORIGIN: " + JSON.stringify(message));
      log.debug("ORIGIN: " + JSON.stringify(headers));
      log.debug("ORIGIN: " + JSON.stringify(deliveryInfo));

      if (!message.originId) {
        log.info('No originId provided. Nothing to do...');
        return next();
      }

      var folder = path.join(mainFolder, 'origins', message.originId);
      createFolder(folder, function() {
        fs.move(message.src.path, path.join(folder, message.src.name),
          function(err) {
            if (err) {
              log.error('Error moving file ' + message.src.path + ' to ' +
                path.join(folder, message.src.name));
              fs.unlink(message.src.path);
              message.error = true;
            }
            message.path = folder;
            message.type = headers.type;
            database.origins.add(message, next);
          });
      });
    });

  workflow.workers.connectWorker('appworker:newproject',
    function onNewProject(message, headers, deliveryInfo, messageObject, next) {
      log.debug("PROJECT: " + JSON.stringify(message));
      log.debug("PROJECT: " + JSON.stringify(headers));
      log.debug("PROJECT: " + JSON.stringify(deliveryInfo));

      var folder = path.join(mainFolder, 'projects', message.projectId);
      createFolder(folder, function() {
        message.path = folder;
        database.projects.add(headers.user, message, next);
      });
    });

  workflow.workers.connectWorker('appworker:updateproject',
    function onUpdProject(message, headers, deliveryInfo, messageObject, next) {
      log.debug("PROJECT UPDATE: " + JSON.stringify(message));
      log.debug("PROJECT UPDATE: " + JSON.stringify(headers));
      log.debug("PROJECT UPDATE: " + JSON.stringify(deliveryInfo));

      var projectData = message.data;
      projectData.projectId = message.projectId;
      database.projects.update(headers.user, projectData, next);
    });

  workflow.workers.connectWorker('appworker:delproject',
    function onDelProject(message, headers, deliveryInfo, messageObject, next) {
      log.debug("PROJECT DELETE: " + JSON.stringify(message));
      log.debug("PROJECT DELETE: " + JSON.stringify(headers));
      log.debug("PROJECT DELETE: " + JSON.stringify(deliveryInfo));

      var folder = path.join(mainFolder, 'projects', message.projectId);
      deleteFolder(folder, function() {
        database.projects.delete(headers.user, message.projectId, next);
      });
    });

  workflow.workers.connectWorker('appworker:newchannel',
    function onNewChannel(message, headers, deliveryInfo, messageObject, next) {
      log.debug("CHANNEL: " + JSON.stringify(message));
      log.debug("CHANNEL: " + JSON.stringify(headers));
      log.debug("CHANNEL: " + JSON.stringify(deliveryInfo));

      var folder = path.join(message.project.path, message.channel.channelId);
      createFolder(folder, function() {
        message.channel.path = folder;
        database.projects.addChannel(headers.user, message.project.projectId,
          message.channel);
        next();
      });
    });

  workflow.workers.connectWorker('appworker:updatechannel',
    function onNewChannel(message, headers, deliveryInfo, messageObject, next) {
      log.debug("CHANNEL: " + JSON.stringify(message));
      log.debug("CHANNEL: " + JSON.stringify(headers));
      log.debug("CHANNEL: " + JSON.stringify(deliveryInfo));

      var folder = path.join(message.project.path, message.channel.channelId);
      createFolder(folder, function() {
        message.channel.path = folder;
        database.projects.updateChannel(headers.user, message.project.projectId,
          message.channel);
        next();
      });
    });

  workflow.workers.connectWorker('appworker:delchannel',
    function onDelChannel(message, headers, deliveryInfo, messageObject, next) {
      log.debug("CHANNEL DELETE: " + JSON.stringify(message));
      log.debug("CHANNEL DELETE: " + JSON.stringify(headers));
      log.debug("CHANNEL DELETE: " + JSON.stringify(deliveryInfo));

      var folder = path.join(mainFolder, 'projects', message.projectId,
        message.channelId);
      deleteFolder(folder, function() {
        database.projects.delChannel(headers.user, message.projectId,
          message.channelId, function () { next(); });
      });
    });

  log.debug('AppWorker started !');
}
