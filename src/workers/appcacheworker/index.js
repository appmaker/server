'use strict';

var log = require('AppMakerLogManager')('AppCacheWorker'),
    config = require('AppMakerCFGManager'),
    database = require('AppMakerDBManager'),
    workflow = require('AppMakerWFManager'),
    appcache = require('./lib/appcache'),
    fs = require('fs-extra'),
    path = require('path');

require('AppMakerProcessManager')('AppCacheWorker');

log.debug('Starting worker...');
config.init(function onCFGLoaded(cfg) {
  database.init(cfg.redis, function onDBConnected() {
    log.debug('DB started (ok)');
    workflow.init(cfg.rabbit, function onWFInit() {
      log.debug('WF started (ok)');
      startWorker();
    });
  });
});

function startWorker() {
  workflow.workers.connectWorker('appcacheworker',
    function onNewMsg(message, headers, deliveryInfo, messageObject, next) {
      log.debug("APPCACHE: " + JSON.stringify(message));
      log.debug("APPCACHE: " + JSON.stringify(headers));
      log.debug("APPCACHE: " + JSON.stringify(deliveryInfo));

      database.projects.get(headers.user,
        message.projectId, function(error, projectData) {
          if (error) {
            log.error("No valid project - " + error + " - " + message.projectId);
            next();
            return;
          }

          var channelData = [];
          if (projectData && projectData.channels) {
            channelData = projectData.channels.filter(function(channel) {
              return channel.channelId === message.channelId;
            });
          }
          if (channelData.length === 0) {
            log.error('No valid channel found !');
            next();
            return;
          }
          channelData = channelData[0];

          fs.copy(
            path.join(__dirname, 'lib/resources/_update_appcache.js'),
            path.join(message.targetPath, '_update_appcache.js'),
            function () {
              appcache.generate({
                output: message.targetPath
              }, next);
            }
          );
        });
    });

  log.debug('AppCacheWorker started !');
}
