'use strict';

var generator = require('node-appcache-generator').Generator;
var fs = require('fs');

var Manifest = function Manifest() {

  var generate = function generate(config, cb) {
    var manifestGenerator = new generator(null, [ '*' ]);
    manifestGenerator.generateFromDir(config.output, function(err, manifest) {
      if (err) {
        throw err;
      }

      fs.writeFile(config.output + '/manifest.appcache',
        manifest, function(err2) {
        if (err2) {
          throw err2;
        }

        if (typeof cb === 'function') {
          cb();
        }
      });

    });
  };

  return {
    'generate': generate
  }
}();

module.exports = Manifest;
