function __updateCache() {
  window.applicationCache.swapCache();
  window.location.reload();
}

window.applicationCache.addEventListener('updateready', __updateCache);
if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
  __updateCache();
}
