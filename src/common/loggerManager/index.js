'use strict';

require('colors');

function log(level, module, msg, optionalObj) {
  if (optionalObj) {
    msg += ':\n' + JSON.stringify(optionalObj, null, ' ').cyan;
  }
  console.log(Date().gray + ' ' + level.bold + ' (' + module + ') ' + ' ' + msg);
}

function checkMessage(msg) {
  if (typeof msg !== 'string') {
    return '---- BAD LOG TRACE ----';
  }
  return msg;
}

global.debug = function(module, msg, optionalObj) {
  log('DEBUG',module, checkMessage(msg).blue, optionalObj);
}

global.info = function(module, msg, optionalObj) {
  log('INFO', module, checkMessage(msg).yellow, optionalObj);
}

global.error = function(module, msg, optionalObj) {
  log('ERROR',module, checkMessage(msg).red, optionalObj);
}

module.exports = function(moduleName) {
  return {
    debug: function(msg, optionalObj) {
      global.debug(moduleName, msg, optionalObj);
    },

    info: function(msg, optionalObj) {
      global.info(moduleName, msg, optionalObj);
    },

    error: function(msg, optionalObj) {
      global.error(moduleName, msg, optionalObj);
    }
  }
};
