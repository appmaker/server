'use strict';

var log = require('AppMakerLogManager')('WFManager'),
    amqp = require('amqp'),
    uuid = require('uuid').v4,
    Promise = require('promise'),
    database = require('AppMakerDBManager');

module.exports = (function WFManager() {
  var config = {},
      _workflows = {},
      connection = null;

  // Load workflows from the database into RAM
  function loadWorkflows(cb) {
    cb = (typeof cb === 'function' ? cb : function() {});

    database.general.getWorkflows(function(err, workflows) {
      if (err) {
        log.error(err);
        cb(err);
      } else {
        log.debug('Caching workflows...');
        _workflows = workflows;
        _workflows['entrypoints'] = [];
        Object.keys(_workflows.workflows).forEach(function(wf) {
          _workflows['entrypoints'].push({
            headers: _workflows.workflows[wf].headers,
            workflow: wf
          });
        });
        cb();
      }
    });
  }

  function connectToAMQP(onReady) {
    connection = amqp.createConnection({
      host: config.host,
      port: config.port,
      login: config.userLogin,
      password: config.userPass,
      vhost: config.virtualhost,
    }, {
      reconnect: true,
      reconnectBackoffStrategy: 'exponential'
    });

    connection.on('ready', onReady);
    connection.on('close', function() {
      log.error('AMQP broker disconnected !');
    });
    connection.on('error', function(err) {
      log.error('AMQP broker error: ' + err);
    });
  }

  function init(cfg, onInit) {
    config = cfg;
    onInit =
      (typeof onInit === 'function' ? onInit : function() {});

    connectToAMQP(function() {
      log.debug('Connected to AMQP broker');
      loadWorkflows(function(err) {
        if (err) {
          log.error('No workflows loaded !');
        }
        log.debug('WFManager Initialized');
        onInit();
      });
    });
  }

  function getWorkflow(headers) {
    var stringifiedHeaders = JSON.stringify(headers),
        wf = null;
    log.debug('Looking for workflow with headers', headers);
    _workflows.entrypoints.forEach(function(entrypoint) {
      // TODO: Use a deep-check method
      log.debug(entrypoint.workflow + ' Headers ', entrypoint.headers);
      if (JSON.stringify(entrypoint.headers) == stringifiedHeaders) {
        var steps = [];
        Object.keys(_workflows.workflows[entrypoint.workflow].flow).forEach(
          function(step) {
            steps.push(step);
          });
        wf = {
          name: entrypoint.workflow,
          wf: _workflows.workflows[entrypoint.workflow],
          steps: steps
        };
      }
    });
    return wf;
  }

  function publishJSONToAMQP(exchangeName, mode, headers, payload) {
    log.debug('publishJSONToAMQP target exchange: ' + exchangeName);
    log.debug('publishJSONToAMQP headers: ', headers);
    log.debug('publishJSONToAMQP payload: ', payload);
    connection.exchange(exchangeName, mode, function(exchange) {
      log.debug('Publising message to workflow ' + exchangeName);
      exchange.publish('', payload, {
        contentType: 'application/json',
        headers: headers
      });
    });
  }
  function dispatchWork(headers, payload, isNewWorkflow) {
    if (!isNewWorkflow) {
      publishJSONToAMQP(headers.workId, {
        type: 'fanout',
        durable: false,
        autoDelete: true
      }, headers, payload);
    }

    if (headers.progress !== 100) {
      publishJSONToAMQP(_workflows.mainflow, {
        type: 'headers',
        durable: true,
        autoDelete: false
      }, headers, payload);
    }
  }

  function advanceWorkflow(workId) {
    log.debug('advanceWorkflow - init ' + workId);
    database.works.getWorkflow(workId).then(function onFullfil(workflowData) {
      log.debug("advanceWorkflow workflowData", workflowData);

      var nextWork = (workflowData.nextWork !== null) &&
                     workflowData.subWorks[workflowData.nextWork];

      if (!nextWork) {
        log.debug('No more works into this workflow');
        return;
      }

      workflowData.nextWork++;
      database.works.saveWorkflow(workId, workflowData).then(function() {
        // Manage work status
        var operation = nextWork.headers.operation;
        var resourceUrl = nextWork.resourceUrl;
        database.works.newStatus(nextWork.id, operation,
          nextWork.resourceUrl);
        var e = connection.exchange(nextWork.id, {
          type: 'fanout'
        });

        connection.queue(nextWork.id, {
          durable: false,
          autoDelete: true
        }, function(q) {
          log.debug('Orchestrating queue ' + q.name + ' created');

          // Dispatching the new workflow
          log.debug('Sending headers', nextWork.headers);
          log.debug('Sending data', nextWork.payload);
          dispatchWork(nextWork.headers, nextWork.payload, true);

          q.bind(e, '*');
          q.subscribe(function (message, headers) {
            database.works.updateStatus(nextWork.id, headers.progress);
            if (headers.progress === 100) {
              log.debug(
                'Work ' + nextWork.id + ' from ' + workId + ' done !');
              q.destroy();
              log.debug('Orchestrating queue ' + q.name + ' destroyed');
              // Dispatch next work of the masterWorkflow
              advanceWorkflow(workId);
            } else {
              log.debug('Work ' + nextWork.id + ' from ' + workId + ' at ' +
                headers.progress + '%');
            }
          });
        });
      });
    }, function onReject(error) {
      log.error('advanceWorkflow ' + workId + ' ERROR ' + error);
    });
  }

  return {
    init: init,
    reload: loadWorkflows,
    get debug() {
      return _workflows;
    },

    producer: {
      getNewWorkId: function() {
        return uuid();
      },

      // Using a metawork of only one subwork
      // Returns a workId
      // optionsObj = {
      //   headers: {},
      //   payload: {},
      //   resourceUrl: resourceUrl
      // }
      addSingleWork: function(loggeduser, optionsObj) {
        var workInfo = this.addWork(loggeduser, optionsObj);
        workInfo.onceAdded.then(function onFullfil() {
          advanceWorkflow(workInfo.workId);
        }, function onReject() {
          log.error('Error processing workflow');
        });
        return workInfo.workId;
      },

      // Adds a new subWork inside a metawork
      // Returns a workId and a promise (onceAdded)
      // optionsObj = {
      //   headers: {},
      //   payload: {},
      //   resourceUrl: resourceUrl
      // }
      addWork: function(loggeduser, optionsObj, metawork) {
        var workId = (metawork ? metawork : this.getNewWorkId());
        var subWorkId = this.getNewWorkId();
        var wf = getWorkflow(optionsObj.headers);
        if (!wf) {
          log.error('No workflow found !');
          return {
            workId: null,
            onceAdded: Promise.reject()
          };
        }
        log.debug('ID ' + subWorkId + ' - workflow to use: ' + wf.name);

        var workData = {
          id: subWorkId,
          parent: workId
        };
        workData.headers = optionsObj.headers;
        workData.headers['next-step'] = wf.steps[0];
        workData.headers['steps'] = JSON.stringify(wf.steps);
        workData.headers['flowlength'] = wf.steps.length;
        workData.headers['progress'] = 0;
        workData.headers['workId'] = subWorkId;
        workData.headers['user'] = loggeduser;
        workData.payload = optionsObj.payload;
        workData.resourceUrl = optionsObj.resourceUrl;
        var promise = database.works.addSubWork(workId, subWorkId, workData);

        return {
          workId: workId,
          onceAdded: promise
        };
      },

      dispatch: function(workId) {
        advanceWorkflow(workId);
      }
    },

    workers: {
      // Connects the worker to the workflow engine.
      // Callback "onMessage" will be called when new work has to be done.
      //  this callback will receive a workObject
      connectWorker: function(workerName, onMessage) {
        log.debug('connectWorker: ' + workerName);
        var q = connection.queue(workerName, {
          durable: true,
          autoDelete: false
        });
        q.subscribe({
            ack: true,
            prefetchCount: config.parallelWorks
          },
          function (message, headers, deliveryInfo, messageObject) {
            onMessage(message, headers, deliveryInfo, messageObject,
              function next(payload) {
                messageObject.acknowledge(false);

                if (payload && typeof payload === 'object') {
                  Object.keys(payload).forEach(function(key) {
                    message[key] = payload[key];
                  });
                }
                var steps = JSON.parse(headers.steps);
                steps.shift();
                headers.steps = JSON.stringify(steps);

                if (steps.length > 0) {
                  headers['next-step'] = steps[0];
                }
                headers.progress = ((headers.flowlength - steps.length) /
                                    headers.flowlength * 100);
                log.debug("send2NextWorker -> " + headers['next-step']);
                dispatchWork(headers, message);
              });
        });
      }
    }
  }
}());
