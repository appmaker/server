'use strict'

var log = require('AppMakerLogManager')('CFGManager'),
    fs = require('fs');

module.exports = (function CFGManager() {
  function loadConfigFile(file, cb) {
    fs.exists(file, function (exists) {
      if (exists) {
        log.debug('Loading configuration from ' + file);
        var cfg = require(file);
        Object.keys(cfg).forEach(function(key) {
          process.configuration[key] = cfg[key];
        });
      } else {
        log.debug('configuration file ' + file + ' not found.');
      }
      cb();
    });
  }

  return {
    init: function(cb) {
      cb = (typeof cb === 'function' ? cb : function() {});

      process.configuration = {};
      loadConfigFile(process.cwd() + '/config.default.json', function() {
        loadConfigFile(process.cwd() + '/config.local.json', function() {
          log.debug('Loaded configuration', process.configuration);
          cb(process.configuration);
        });
      });
    }
  }
}());
