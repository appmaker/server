'use strict';

var fs = require('fs'),
    moduleName = 'nodefinedyet',
    pidFile = '/tmp/nodefinedyet';

function recordPID(cb) {
  // On init, we store the PID into /var/run/<moduleName>.pid
  var fs = require('fs'),
      pid = process.pid;

  pidFile = '/var/run/' + moduleName + '.pid';
  fs.writeFile(pidFile, pid, function(e) {
    if (e) {
      pidFile = '/tmp/' + moduleName + '.pid';
      fs.writeFile(pidFile, pid);
      cb();
    } else {
      cb();
    }
  });
}

function registerSignals() {
  function orderedClose() {
    showRunningInfo();
    console.log('Close ' + moduleName + ' in process - unlinking PID file...');
    fs.unlink(pidFile, function() {
      console.log('Closing...');
      process.exit();
    });
  }

  function showRunningInfo() {
    console.log('Hello to the ' + moduleName + ' information console');
    console.log(' * PID ' + process.pid + ' uptime ' + process.uptime());
    var mem = process.memoryUsage();
    console.log(' * Memory: RSS ' + mem.rss + ' Heap ' + mem.heapUsed + ' / ' +
      mem.heapTotal + ' (' + (mem.heapUsed / mem.heapTotal * 100.) + '%)');
  }

  process.on('SIGINT', orderedClose);  // 2
  process.on('SIGTERM', orderedClose); // 15
  process.on('SIGUSR2', showRunningInfo); // 12
}

module.exports = function(_moduleName) {
  moduleName = _moduleName;
  recordPID(function() {
    registerSignals();
  });
};
