'use strict';

var log = require('AppMakerLogManager')('DBManager'),
    Promise = require('promise'),
    redis = require('redis');

module.exports = (function DBManager() {
  var _client = null,
      _ready = false;

  function isConnected() {
    return _client && _client.connected && _ready;
  }

  function hgetall_objects(db, cb) {
    _client.hgetall(db, function(error, data) {
      if (data !== null) {
        Object.keys(data).forEach(function(d) {
          try {
            data[d] = JSON.parse(data[d]);
          } catch(e) {}
        });
      }
      cb(error, data);
    });
  }

  function update(obj, hash) {
    for (var key in hash) {
      if (hash.hasOwnProperty(key)) {
        updateKey(obj, key, hash[key]);
      }
    }
    return obj;
  }

  function updateKey(obj, key, value) {
    if (value === null || typeof value === 'undefined') {
      delete obj[key];
    }
    else if (typeof value !== 'object') {
      obj[key] = value;
    }
    else {
      if (typeof obj[key] !== 'object') {
        obj[key] = {};
      }
      update(obj[key], value);
    }
  }

  function init(cfg, onConnected) {
    onConnected =
      (typeof onConnected === 'function' ? onConnected : function() {});

    if (isConnected()) {
      log.debug('Connected, no init needed');
      onConnected();
    };

    _client = redis.createClient();
    hset = Promise.denodeify(_client.hset.bind(_client));
    hdel = Promise.denodeify(_client.hdel.bind(_client));
    hget = Promise.denodeify(_client.hget.bind(_client));

    _client.on('connect', function () {
      log.debug('Connected');
      _client.select(cfg.database, function() {
        _ready = true;
        log.debug('DBManager Initialized');
        onConnected();
      });
    });
    _client.on('disconnect', function () {
      _ready = false;
      log.debug('Disconnected');
    });
    _client.on('error', function (err) {
      log.error('Error: ' + err);
    });
  }

  function parse(cb) {
    return function (err, str) {
      try {
        if (!str) {
          cb('Not found');
        } else {
          cb(err, !err ? JSON.parse(str) : undefined);
        }
      }
      catch(e) {
        cb(e);
      }
    }
  }

  var hset, hget, hdel;

  var database = {
    init: init,

    get isConnected() {
      return isConnected();
    },

    sessions: {
      get: function(sessionId) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        return hget('sessions', sessionId).then(function (sessionData) {
          return JSON.parse(sessionData);
        });
      },

      set: function(sessionId, sessionData) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        sessionData = sessionData || {};
        sessionData.lastUpdate = Date.now();
        return hset('sessions', sessionId, JSON.stringify(sessionData));
      }
    },

    authentications: {
      get: function(authId) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        return hget('authentications', authId).then(function (authData) {
          return JSON.parse(authData);
        });
      },

      set: function(authId, authData) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        return hset('authentications', authId, JSON.stringify(authData));
      }
    },

    general: {
      existDeploymentType: function(deploymentType, cb) {
        cb = (typeof cb === 'function') ? cb : function() {};
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hexists('deploymentTypes',deploymentType, cb);
        }
      },

      getOriginTypes: function(cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('originTypes', cb);
        }
      },

      getDeploymentTypes: function(cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('deploymentTypes', cb);
        }
      },

      getPermissions: function(cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('permissions', cb);
        }
      },

      getPlatforms: function(cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('platforms', cb);
        }
      },

      getWorkflows: function(cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('workflows', cb);
        }
      }
    },

    subdomains: {
      addSubdomain: function (subdomain) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        subdomain = this._normalizeSubdomain(subdomain);
        // TODO: 'client' should be provided in configuration
        // See issue https://gitlab.com/appmaker/server/issues/60
        if (subdomain === 'client' || typeof subdomain !== 'string') {
          return Promise.reject('invalid-subdomain');
        }
        return hget('subdomains', subdomain)
          .then(function (value) {
            if (value) { return Promise.reject('subdomain-already-exists'); }
            var subdomainObject = JSON.stringify({ subdomainId: subdomain });
            return hset('subdomains', subdomain, subdomainObject);
          });
      },

      removeSubdomain: function (subdomain) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }

        // TODO: 'client' should be provided in configuration
        // See issue https://gitlab.com/appmaker/server/issues/60
        if (subdomain === 'client' || typeof subdomain !== 'string') {
          return Promise.reject('invalid-subdomain');
        }
        subdomain = this._normalizeSubdomain(subdomain);
        return hget('subdomains', subdomain)
          .then(function (value) {
            if (value) { return hdel('subdomains', subdomain); }
          });
      },

      _normalizeSubdomain: function (subdomain) {
        return typeof subdomain === 'string' ?
               subdomain.toLowerCase() : subdomain;
      }
    },

    works: {
      addSubWork: function(workId, subWorkId, workData) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        var self = this;
        return self.getWorkflow(workId).then(function(workflowData) {
          workflowData.subWorks.push(workData);
          if (workflowData.nextWork === null) {
            workflowData.nextWork = 0;
          }
          return self.saveWorkflow(workId, workflowData);
        });
      },

      getWorkflow: function(workId) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        return new Promise(function(resolve, reject) {
          _client.hget('works', workId, function (err, data) {
            if (err) {
              return reject(err);
            }
            // TODO: Reject when no exists. ISSUE #38
            if (!data) {
              resolve({
                id: workId,
                nextWork: null,
                subWorks: []
              });
            }
            try {
              resolve(JSON.parse(data));
            } catch(e) {
              reject(e);
            }
          });
        });
      },

      saveWorkflow: function(workId, workflowData) {
        if (!_client.connected) {
          return Promise.reject('No database connection!');
        }
        return hset('works', workId, JSON.stringify(workflowData));
      },

      newStatus: function(workId, operation, resourceUrl, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hset('workStatus', workId, JSON.stringify({
            progress: 0,
            operation: operation,
            resourceUrl: resourceUrl,
            lastUpdate: Date.now()
          }), cb);
        }
      },

      updateStatus: function(workId, completed, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('workStatus', workId, function (err, data) {
            if (err) { return cb(err); }
            var json = JSON.parse(data);
            json.progress = completed;
            if (completed === 100) {
              json.status = 0; // TODO: this should be an argument, 0 means OK
            }
            json.lastUpdate = Date.now();
            _client.hset('workStatus', workId, JSON.stringify(json), cb);
          });
        }
      },

      getStatus: function(workId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('workStatus', workId, parse(cb));
        }
      }
    },

    origins: {
      get add() {
        return this.update;
      },

      update: function(originData, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hset('origins', originData.originId,
            JSON.stringify(originData), cb);
        }
      },

      get: function(originId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('origins', originId, parse(cb));
        }
      }
    },

    projects: {
      getAll: function getAllProjects(loggedUser, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('projects_' + loggedUser, normalize);
          // Only needs but hmget is not implemented
          //  ['name', 'description', 'iconURL', 'gitRepository'];
        }

        function normalize(err, data) {
          if (err) { return cb(err); }

          data = data || {};
          Object.keys(data).forEach(function (key) {
            var currentProject = data[key];
            currentProject.channels = currentProject.channels || {};
            var ids = Object.keys(currentProject.channels);
            currentProject.channels = ids.map(function (channelId) {
              //TODO: please, refactor this to avoid side effects on a map()
              var channel = currentProject.channels[channelId];
              return channel;
            });
          });

          cb(null, data);
        }
      },

      getAllProjectIds: function getAllProjectIds(loggedUser, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hkeys('projects_' + loggedUser, cb);
        }
      },

      get: function getProject(loggedUser, projectId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('projects_' + loggedUser, projectId, parse(normalize));
        }

        function normalize(err, json) {
          if (!json || err) { return cb(err || 'no-data'); }

          json.channels = json.channels || {};
          json.channels = Object.keys(json.channels).map(function (id) {
            return json.channels[id];
          });
          cb(null, json);
        };
      },

      exists: function existsProject(loggedUser, projectId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hexists('projects_' + loggedUser, projectId, cb);
        }
      },

      getProjectByName: function(loggedUser, projectName, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          hgetall_objects('projects_' + loggedUser, findProject);
        }

        function findProject(error, data) {
          if (error) {
            console.log(error);
            cb(error);
            return;
          }
          var project = null;
          if (data) {
            var projectId = Object.keys(data).filter(function (key) {
              return data[key].name === projectName;
            });
            if (projectId && projectId.length > 0) {
              project = data[projectId[0]];
            }
          }
          cb(null, project);
        }
      },

      add: function(loggedUser, projectInfo, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          this.exists(loggedUser, projectInfo.projectId, function(error, data) {
            if (error) {
              log.error(error);
              cb(error);
            } else if (data === 1) {
              log.debug('Project ' + projectInfo.projectId +
                    ' for user ' + loggedUser + ' already exists');
              cb('Already exists');
            } else {
              _client.hset('projects_' + loggedUser, projectInfo.projectId,
                JSON.stringify(projectInfo), cb);
            }
          });
        }
      },

      update: function(loggedUser, projectInfo, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('projects_' + loggedUser, projectInfo.projectId,
            function(error, data){
              if (error || !data) {
                error = error || 'No project found';
                log.error(error);
                cb(error);
              } else {
                var prjData = JSON.parse(data);
                Object.keys(projectInfo).forEach(function(key) {
                  prjData[key] = projectInfo[key];
                });
                _client.hset('projects_' + loggedUser, projectInfo.projectId,
                  JSON.stringify(prjData), cb);
                log.debug("Project updated", prjData);
              }
            });
        }
      },

      delete: function(loggedUser, projectId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hdel('projects_' + loggedUser, projectId, cb);
        }
      },

      getChannel: function(username, projectId, channelId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          return cb('No database connection!');
        }
        _client.hget('projects_' + username, projectId,
          function (error, data) {
            if (error) {
              log.error(error);
              return cb(error);
            }
            var projectData = JSON.parse(data);
            if (!projectData) {
              var error = 'No data found for project ' + projectId;
              log.error(error);
              return cb(error);
            }
            projectData.channels = projectData.channels || {};
            var channel = projectData.channels[channelId];
            if (!channel) {
              var error = 'Channel ' + channelId + ' not found';
              log.error(error);
              return cb(error);
            }
            return cb(null, channel);
          }
        );
      },

      addChannel: function(loggedUser, projectId, channelInfo, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('projects_' + loggedUser, projectId,
            function(error, data) {
              if (error) {
                log.error(error);
                cb(error);
              } else {
                var prjData = JSON.parse(data);
                if (!prjData) {
                  var error = 'No data found for project ' + projectId;
                  log.error(error);
                  cb(error);
                } else {
                  if (!prjData.channels) {
                    prjData.channels = {};
                  }
                  prjData.channels[channelInfo.channelId] = channelInfo;
                  _client.hset('projects_' + loggedUser, projectId,
                    JSON.stringify(prjData), cb);
                }
              }
            });
        }
      },

      updateChannel: function(loggedUser, projectId, channelInfo, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('projects_' + loggedUser, projectId,
            function(error, data) {
              if (error) {
                log.error(error);
                cb(error);
              } else {
                var prjData = JSON.parse(data);
                if (!prjData) {
                  var error = 'No data found for project ' + projectId;
                  log.error(error);
                  cb(error);
                } else {
                  if (!prjData.channels) {
                    prjData.channels = {};
                  }
                  var channel = prjData.channels[channelInfo.channelId];
                  if (!channel) { return cb('channel-not-found'); }
                  if (typeof channel !== 'object') {
                    return cb('inconsistent-data-in-channel');
                  }
                  update(channel, channelInfo);
                  _client.hset('projects_' + loggedUser, projectId,
                    JSON.stringify(prjData), cb);
                }
              }
            });
        }
      },

      delChannel: function(loggedUser, projectId, channelId, cb) {
        cb = (typeof cb === 'function' ? cb : function() {});
        if (!_client.connected) {
          cb('No database connection!');
        } else {
          _client.hget('projects_' + loggedUser, projectId,
            function(error, data) {
              if (error) {
                log.error(error);
                cb(error);
              } else {
                var prjData = JSON.parse(data);
                if (!prjData || !prjData.channels) {
                  var error = 'No data found for project ' + projectId;
                  log.error(error);
                  cb(error);
                } else {
                  var channel = prjData.channels[channelId];
                  var deployment = channel && channel.deployment;
                  var releaseResources = Promise.resolve();
                  if (deployment && deployment.type === 'appmaker') {
                    var subdomain = deployment.params &&
                                    deployment.params.subdomain;
                    releaseResources =
                      database.subdomains.removeSubdomain(subdomain);
                  }
                  releaseResources.then(function () {
                    delete prjData.channels[channelId];
                    _client.hset('projects_' + loggedUser, projectId,
                      JSON.stringify(prjData), cb);
                  });
                }
              }
            }
          );
        }
      }
    }
  };

  return database;
}());
